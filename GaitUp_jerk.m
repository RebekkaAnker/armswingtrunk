function [ jerk, Njerk ] = GaitUp_jerk( data )
%GAITUP_JERK computes the integrated squared jerk (Smith,2000) and
%normalized jerk by the acceleration excursion
%   Detailed explanation goes here

for i=1:length(data)
acc(i,1)= norm(data(i,2:3));
end

jerk = sum(diff(acc).^2);
Njerk = (jerk - min(acc))/(max(acc)-min(acc));
end

