function result=GaitUpCore1_2c_csv(options,sensor1,sensor2)
% GaitUp version 1.2c
% Out=GaitUpCore1_2c({0,0,27,1,0})
% Benoit Mariani, GAIT UP
% benoit.mariani@gaitup.com

%last modified:
% 04.05.2015 by Benoit
% - l.920-960 in Synchroniser: add robustness by detecting Radio channel
% 3.11.2014 by Benoit
% -  l.641 corrected conversion in seconds for 5 (1:5) columns instead of one
% -  l.651 added HsL to the export (was missing before) (1,4,9:end),
% corrected also l 728 and 730 correspondignly
% - l.741 symettry calculation start at gct (line 4)
% 11.09.2014 by Benoit
% - l. 911-958 Update Synchroniser function to be robust with Master not
% starting at 1.
%19.06.2014 by Rebekka
% - l. 128 moved synchronization after calibration, solves problem of foot
% clearance sometimes being NaN when 2 files analyzed together
% 05.05.2014 by Arnaud
% - Version number of the script set to 1.2 as the revision number for the
%   C++ code
% - l.518,522 Definition of the variables result.(NGaitCycles|GaitTime) reset to the
%   orginal code until the rapport V2 will be implemented in the C++ code.
% - l.457-508: The option used in "Turning And Initiation Detection" were
%   reactivated until the rapport V2 will be implemented in the C++ code.
% 30.4.2014 by Rebekka
% - l.161-165 added test for which foot makes first step to not lose a
%   cycle at the start
% - l.161-170 add variable 'First' to keep track of order in OutRL
% - l.405-426 when adding parameters into OutRL test 'First' for corresponding sides
% 30.4.2014 by Benoit
% - l.151 Updated value for checking correspondance from 2sec to 5sec
% - l.392-408 Integration of parameters in OutRL structure when two feet available
% - l.466 abd 525 New tests data for "essential" report
% - l.410 Changed turn and init discarding strategy: first detection i Out or OutRL, thend after updated STATS
% - l.486/571 add Added Heel-strike absolute time in cycle results
% - l512/670 Added Ratio Symetry measure in Statistics and updated CV
% calculus
% 24.4.2014 by Rebekka
% - Started fixing problem when not the same number of cycles between feet
% - Commented step time, length and asymmetry parameters (if used cause
% problem because add cycle were no one is as for DS before)
% - when 2 files analysed at the same time the CV is not calculated
% 24.4.2014 by Arnaud
% - Data resampled if sensors' sample frequency is not 200Hz
% - No more need to read a second time the file to synchronize the sensors
% - Fix the arrangement of the steps in the table (used to store the cycles' data) when
%   one foot has at least two more steps than the other
% 23.4.2014 by Arnaud
% - Simplification of the code related to the previous modification
% - The length of the vector used to initialize TO, MS, and HS is wrong
% 23.4.2014 by Rebekka : take TO, MS, HS in ]FF1,FF2[ instead of ]FF1,FF2]
% 14.4.2014 by Rebekka : additional test for ensuring gait event order
% 04.04.2014 by Arnaud
% - Since the removing of unvalid cycle (see section 'Associate features to cycles'),
%   it is no more possible to use the length of the MotionLess variable in the function
%   GaitUp_Clearance. It is replaced by length(Cycle).
% - Left/Right detection using physilogs' filename implemented
% - Function 'synchronizer' rewritten to use the sensor header to detect the slave and
%   master sensor. The function resizes also the IMUs' signals to have the same number
%   of frames.
% - The GaitTime parameter for the case with 2 sensors was not corrected as for the one
%   sensor case (i.e +1 was missing in gt=lf-ff+1)
% - uigetfile('*.bin') replaced by uigetfile('.BIN'): MacOS X case sensitive
% - file path are not valid under MacOS X
%26/03/2014, corrections by Benoit and Arnaud
%2.4.2014, Rebekka correction

% Code of the 04.05.2015 modified to work with CSV files generated from the
% RTK
%% Preliminary
% retrieve options from interface :
Turn=cell2mat(options(1)); % boolean "Discard Turns" (default value 1)
Init=cell2mat(options(2)); % integer "Discard Initiation & Termination" // How many: 1, 2... (default value 2)
Clea=cell2mat(options(3)); % float "Analyse Foot Clearance" // Shoe size: xx cm (default value 27cm)
Save=cell2mat(options(4)); % boolean "Save Results in xls file" (default value yes)
Expo=cell2mat(options(5)); %boolean "Export raw data in csv file" (default value no)
UrlGaitUp = 'http://www.gaitup.com'; %put link to site with errors, to be created
UrlGaitUpContact = 'http://www.gaitup.com/contact/';
% other options (not in this version): Segmentation (1/0), Display (1/0)

%% File Loading
% declare internal variables
Signal=[];
SensorN =2;

% % Ask user to select recorded file
% [filename, pathname] = uigetfile('*.csv','Select the .csv files for the feet','MultiSelect', 'on');
% 
% % Count number of files selected by user
% if ~ischar(filename)
%     SensorN=length(filename);
% else
%     filename={filename}; % convert string to cell for compatibility later
%     SensorN=1;
% end
% 
% 
% 
% % File(s) loading in Gait Up
% if SensorN<1 || SensorN>2 % if user didn't select 1 or 2 files
%     fprintf('Warning: please chose either 1 or 2 Physilog files from gait recording \n');
%     return
% else
%     for i=1:SensorN % for 1 or both sides
% %         file=[pathname, filename{i}];
% %         fprintf(['file ' filename{i} ' loaded \n']);
% %         
% %         [sensor]=importcsvfileRTK(file,segmented);
% 
%         while length(sensor(1).data)~= length(sensor(2).data) % adjust sample number if not the same (acc. vs. gyro)
%             if length(sensor(1).data)>length(sensor(2).data)
%                 sensor(1).data=sensor(1).data(1:end-1,:); %delete last sample where more
%             else
%                 sensor(2).data=sensor(2).data(1:end-1,:);
%             end
%             %fprintf('adjust sample number')
%         end
        Signal(1).S = [sensor1(1).data, sensor1(2).data];
        Signal(1).fs = 200; % sampling frequency
        Signal(1).sensor = sensor1;
%         Signal(1).file=file;
        
        Signal(2).S = [sensor2(1).data, sensor2(2).data];
        Signal(2).fs = 200; % sampling frequency
        Signal(2).sensor = sensor2;
%         Signal(2).file=file;
        
        
%     end
    % Synchronization files -> Moved after Calibration
    %     if (SensorN == 2)
    %         [Signal(1).S, Signal(2).S] = synchronizer(Signal);
    %     end
% end
%% Segmentation : not in this version, use the entire signal

%% Calibration and Temporal Detection
for i=1:SensorN % for 1 or both sides
    
    %Contains correction with calibration values
    Signal(i).S=GaitUp_Calibration(Signal(i).S);
end

% Synchronization files
if (SensorN == 2)
%     [Signal(1).S, Signal(2).S] = synchronizer(Signal);
end

for i=1:SensorN
    % Temporal Analysis
    Out(i)=GaitUp_Temporal(Signal(i).S);
    
    %     %%% TO CHECK VISUALY:
%     GaitUp_Temporal_Display(Out(i),Signal(i));
end

% %%Display calibrated and synchronized raw data
% figure;
% plot(Signal(1).S(:,1:3));
% figure;
% plot(Signal(1).S(:,4:6));

% compute double support if two sensors are available
if SensorN==2
    %Display second signal if existing
    %plot(Signal(2).S(:,5),'r');
    
    %%%%CHECK IF CORRESPONDING FILES%%%%
    %Test if midswing of second and last cycle are not further away than 5 sec
    % %between the feet.
    if abs(Out(1).Cycle(2).MS - Out(2).Cycle(2).MS) > 5*200 || abs(Out(1).Cycle(end).MS - Out(2).Cycle(end).MS) > 5*200
        fprintf(['Error 1 (Files do not correspond), details can be found on our website: ' UrlGaitUp '\n']);
        %web(UrlGaitUp);
        %return
    end
    
    if Out(1).Cycle(1).HS<Out(2).Cycle(1).HS % initiation with foot # 1
        OutRL=GaitUp_BiTemporal(Out(1),Out(2));
        First=1;
    else % initiation with foot # 2
        OutRL=GaitUp_BiTemporal(Out(2),Out(1));
        First=NaN;
    end
    %     % Initialize with NaN values to avoid empty fields
    %     for i=1:length(Out(1).Cycle)
    %         Out(1).Cycle(i).DS=nan;
    %     end
    %     for i=1:length(Out(2).Cycle)
    %         Out(2).Cycle(i).DS=nan;
    %     end
    
    %     if Out(1).Cycle(1).HS<Out(2).Cycle(1).HS % initiation with foot # 1
    %         OutRL=GaitUp_BiTemporal(Out(1),Out(2));
    %         %         Out(1).Cycle(1)
    %         %         Out(2).Cycle(1)
    %         First=1;
    %         % Report double support in Right and Left structures
    %         %DSv=[OutRL.Cycle.DS];
    %         for i=1:length(OutRL.Cycle)
    %             if i<=length(Out(1).Cycle)
    %                 if Out(1).Cycle(i).HS == OutRL.HsR(i)
    %                     Out(1).Cycle(i).DS = OutRL.Cycle(i).DS;
    %                 else
    %                     Out(1).Cycle(i).DS = NaN;
    %                 end
    %             end
    %             if i<= length(Out(2).Cycle)
    %                 if Out(2).Cycle(i).HS == OutRL.HsL(i)
    %                     Out(2).Cycle(i).DS = OutRL.Cycle(i).DS;
    %                 else
    %                     Out(2).Cycle(i).DS = NaN;
    %                 end
    %             end
    %             %             [Out(1).Cycle(i).DS]=DSv(i);
    %             %             [Out(2).Cycle(i).DS]=DSv(i);
    %         end
    %     else % initiation with foot # 2
    %         OutRL=GaitUp_BiTemporal(Out(2),Out(1));
    %         First=2;
    %         % Report double support in Right and Left structures
    %         DSv=[OutRL.Cycle.DS];
    %         for i=1:length(OutRL.Cycle)
    %             if i<= length(Out(2).Cycle)
    %                 if Out(2).Cycle(i).HS == OutRL.Cycle(i).HsR
    %                     Out(2).Cycle(i).DS = OutRL.Cycle(i).DS;
    %                 else
    %                     Out(2).Cycle(i).DS = NaN;
    %                 end
    %             end
    %             if i<=length(Out(1).Cycle)
    %                 if Out(1).Cycle(i).HS == OutRL.Cycle(i).HsL
    %                     Out(1).Cycle(i).DS = OutRL.Cycle(i).DS;
    %                 else
    %                     Out(1).Cycle(i).DS = NaN;
    %                 end
    %             end
    %             %             [Out(1).Cycle(i).DS]=DSv(i);
    %             %             [Out(2).Cycle(i).DS]=DSv(i);
    %         end
    %     end
end

%% Spatial Analysis
if SensorN==1
    Out=GaitUp_Spatial(Out);
%        GaitUp_Spatial_Display(Out);
else
    Out=[GaitUp_Spatial(Out(1)) GaitUp_Spatial(Out(2))];

    % Detection using Physilogs' filename
%     % -----------------------------------
%     if ((strcmp(filename{1}(1:2),'LF') == 1) && (strcmp(filename{2}(1:2),'RF') == 1))
%         %  2 is right foot
%         OutR=Out(2);
%         OutL=Out(1);
%         Side=1;
%     elseif ((strcmp(filename{1}(1:2),'RF') == 1) && (strcmp(filename{2}(1:2),'LF') == 1))
%         % 1 is right foot
%         OutR=Out(1);
%         OutL=Out(2);
%         Side=NaN;
%     else
        % Automatic detection of right and left foot if there are two sensors
        % -----------------------------------
        if nanmedian([Out(1).Cycle.swidth])<0 && nanmedian([Out(2).Cycle.swidth])>0 % then 1 is right foot
            OutR=Out(1);
            OutL=Out(2);
            Side=NaN;
        else
            if nanmedian([Out(1).Cycle.swidth])>0 && nanmedian([Out(2).Cycle.swidth])<0 % then 2 is right foot
                OutR=Out(2);
                OutL=Out(1);
                Side=1;
            else % wrong measure of Swing width, right and left foot undetected, random choice > give warning to user ?
                fprintf(['Error 3 (Wrong measure of swing width), contact our support team: ' UrlGaitUp '\n']);
                OutR=Out(1);
                OutL=Out(2);
                Side=NaN;
            end
        end
%     end
    
    clear Out
    
%     GaitUp_Spatial_Display(OutL);
%     GaitUp_Spatial_Display(OutR);
end

%% Clearance Analysis
if Clea>0
    if SensorN==1
        Out=GaitUp_Clearance(Out,Clea);
    else
        OutR=GaitUp_Clearance(OutR,Clea);
        OutL=GaitUp_Clearance(OutL,Clea);
    end
end

%% Step time and length     (added 24.9.2013-1.10.2013 by Rebekka)
% if SensorN ==2
%     %Temporal
%     %initialisation of the temporal variables
%     for i=1:length(OutL.Cycle)
%         OutL.Cycle(i).stept=NaN;
%     end
%     for i=1:length(OutR.Cycle)
%         OutR.Cycle(i).stept=NaN;
%     end
%
%     if First==1 %initiation with foot #1 (variable declared in %%Calibration and Temporal Detection)
%         if Side ==1 %left is #1 (variable declared in %%Spatial Analysis)
%             for i=1:length(OutRL.Cycle)
%                 OutL.Cycle(i).stept=OutRL.Cycle(i).Rstept; %R is always first to match code Arash (in BiTemporal)
%                 OutR.Cycle(i).stept=OutRL.Cycle(i).Lstept;
%             end
%         else %right foot is #1 or unknown
%             for i=1:length(OutRL.Cycle)
%                 OutL.Cycle(i).stept=OutRL.Cycle(i).Lstept;
%                 OutR.Cycle(i).stept=OutRL.Cycle(i).Rstept;
%             end
%         end
%     elseif First==2 %initiation with foot #2
%         if Side ==1 %left is #1
%             for i=1:length(OutRL.Cycle)
%                 OutL.Cycle(i).stept=OutRL.Cycle(i).Lstept;
%                 OutR.Cycle(i).stept=OutRL.Cycle(i).Rstept;
%             end
%         else %right foot is #1 or unknown
%             for i=1:length(OutRL.Cycle)
%                 OutL.Cycle(i).stept=OutRL.Cycle(i).Rstept;
%                 OutR.Cycle(i).stept=OutRL.Cycle(i).Lstept;
%             end
%         end
%     end
%
%     %Spatial
%     %initialisation of the spatial variables
%     for i=1:length(OutL.Cycle)
%         OutL.Cycle(i).stepl=NaN;
%     end
%     for i=1:length(OutR.Cycle)
%         OutR.Cycle(i).stepl=NaN;
%     end
%
%
%     %Second method using step time * speed (speed = mean of norm of x,y about one
%     %cycle)
%     %     for i=1:length(OutRL.Cycle)
%     %     OutR.Cycle(i).stepl=OutR.Cycle(i).stept*OutR.Cycle(i).speed;
%     %     OutL.Cycle(i).stepl=OutL.Cycle(i).stept*OutL.Cycle(i).speed;
%     %     end
%
%     %Third method using Stride lengths
%     % Problem if one step is wrong due to turn. Recursive manner to calculate
%     % falsifies all following results. Clarify in protocol and put NaN values
%     % after turn.
%     if First==1 %initiation with foot #1
%         if Side==1 %left foot is #1
%             OutL.Cycle(1).stepl=OutL.Cycle(1).slength;
%             OutR.Cycle(1).stepl=OutR.Cycle(1).slength-OutL.Cycle(1).stepl;
%             for i=2:min(length(OutR.Cycle), length(OutL.Cycle))
%                 OutL.Cycle(i).stepl=OutL.Cycle(i).slength-OutR.Cycle(i-1).stepl;
%                 OutR.Cycle(i).stepl=OutR.Cycle(i).slength-OutL.Cycle(i).stepl;
%             end
%         else %right foot is #1
%             OutR.Cycle(1).stepl=OutR.Cycle(1).slength;
%             OutL.Cycle(1).stepl=OutL.Cycle(1).slength-OutR.Cycle(1).stepl;
%             for i=2:min(length(OutR.Cycle), length(OutL.Cycle))
%                 OutR.Cycle(i).stepl=OutR.Cycle(i).slength-OutL.Cycle(i-1).stepl;
%                 OutL.Cycle(i).stepl=OutL.Cycle(i).slength-OutR.Cycle(i).stepl;
%             end
%         end
%     elseif First==2 %initiation with foot #2
%         if Side==1 %left foot is #1
%             OutR.Cycle(1).stepl=OutR.Cycle(1).slength;
%             OutL.Cycle(1).stepl=OutL.Cycle(1).slength-OutR.Cycle(1).stepl;
%             for i=2:min(length(OutR.Cycle), length(OutL.Cycle))
%                 OutR.Cycle(i).stepl=OutR.Cycle(i).slength-OutL.Cycle(i-1).stepl;
%                 OutL.Cycle(i).stepl=OutL.Cycle(i).slength-OutR.Cycle(i).stepl;
%             end
%         else %right foot is #1
%             OutL.Cycle(1).stepl=OutL.Cycle(1).slength;
%             OutR.Cycle(1).stepl=OutR.Cycle(1).slength-OutL.Cycle(1).stepl;
%             for i=2:min(length(OutR.Cycle), length(OutL.Cycle))
%                 OutL.Cycle(i).stepl=OutL.Cycle(i).slength-OutR.Cycle(i-1).stepl;
%                 OutR.Cycle(i).stepl=OutR.Cycle(i).slength-OutL.Cycle(i).stepl;
%             end
%         end
%     end
%
%     %%ASYMMETRY MEASURES
%     %initialisation of the variables
%     for i=1:length(OutL.Cycle)
%         OutL.Cycle(i).stepAS_temp=NaN; %put them at the end of the list
%         OutL.Cycle(i).stepAS_spat=NaN;
%     end
%
%     for i=1:min(length(OutL.Cycle), length(OutR.Cycle))%TEMPORAL
%         %[OutL.Cycle(i).stepAS_temp] = 100*(OutR.Cycle(i).stept-OutL.Cycle(i).stept)./(0.5*(OutR.Cycle(i).stept+OutL.Cycle(i).stept)); %Symmetry index by W Herzog
%         [OutL.Cycle(i).stepAS_temp] = OutR.Cycle(i).stept./OutL.Cycle(i).stept; %Simple division of R/L
%     end
%
%     for i=1:min(length(OutL.Cycle), length(OutR.Cycle))%SPATIAL
%         %[OutL.Cycle(i).stepAS_spat]=100*(OutR.Cycle(i).stepl-OutL.Cycle(i).stepl)./(0.5*(OutR.Cycle(i).stepl+OutL.Cycle(i).stepl));   %always 100*(R-L)/0.5(R+L)
%         [OutL.Cycle(i).stepAS_spat]=OutR.Cycle(i).stepl./OutL.Cycle(i).stepl;
%     end
%
%     %%LIMP
%     %initialisation if limp variable
%     for i=1:length(OutL.Cycle)
%         OutL.Cycle(i).limp=NaN;
%     end
%     %calculation of limp
%     for i=1:length(OutRL.Cycle)
%         OutL.Cycle(i).limp=abs(OutRL.Cycle(i).IDS-OutRL.Cycle(i).TDS);
%     end
%
% end
%% OutRL structure construction
if SensorN==2
    for i=1:length(OutRL.Cycle)
        if Side==1 %Out(1)>left
            if First==1 %Out(1) is HsR
                % look for corresponding cycles
                IndexR=find([OutR.Cycle.HS]==OutRL.Cycle(i).HsL);
                IndexL=find([OutL.Cycle.HS]==OutRL.Cycle(i).HsR);
            else
                IndexR=find([OutR.Cycle.HS]==OutRL.Cycle(i).HsR);
                IndexL=find([OutL.Cycle.HS]==OutRL.Cycle(i).HsL);
            end
        else
            if First==1 %Out(1)>left
                % look for corresponding cycles
                IndexR=find([OutR.Cycle.HS]==OutRL.Cycle(i).HsR);
                IndexL=find([OutL.Cycle.HS]==OutRL.Cycle(i).HsL);
            else
                IndexR=find([OutR.Cycle.HS]==OutRL.Cycle(i).HsL);
                IndexL=find([OutL.Cycle.HS]==OutRL.Cycle(i).HsR);
            end
        end
        
        %         if Side==1 %HsR>OutL !
        %             % look for corresponding cycles
        %             IndexR=find([OutR.Cycle.HS]==OutRL.Cycle(i).HsL);
        %             IndexL=find([OutL.Cycle.HS]==OutRL.Cycle(i).HsR);
        %         else
        %             %HsR>OutR !
        %             % look for corresponding cycles
        %             IndexR=find([OutR.Cycle.HS]==OutRL.Cycle(i).HsR);
        %             IndexL=find([OutL.Cycle.HS]==OutRL.Cycle(i).HsL);
        %         end
        Fields=fieldnames(OutR.Cycle);
        
        for f=8:length(Fields)
            eval(['OutRL.Cycle(i).' cell2mat(Fields(f)) '_R=NaN;']);
            eval(['OutRL.Cycle(i).' cell2mat(Fields(f)) '_L=NaN;']);
            if ~isempty(IndexR)
                eval(['OutRL.Cycle(i).' cell2mat(Fields(f)) '_R=OutR.Cycle(IndexR).' cell2mat(Fields(f)) ';']);
            end
            if ~isempty(IndexL)
                eval(['OutRL.Cycle(i).' cell2mat(Fields(f)) '_L=OutL.Cycle(IndexL).' cell2mat(Fields(f)) ';']);
            end
        end
    end
end

%% Turning And Initiation Detection
DiscardC.turns=[];
DiscardC.init=[];
if SensorN==1
    if Turn==1
        DiscardC.turns=find(abs([Out.Cycle.TA])>20);
    end
    if Init>0
        if length([Out.Cycle])>2*Init+1 % otherwise alert:there is not enough cycle to remove initiation and termination
            DiscardC.init=[[1:Init] [length([Out.Cycle])-Init+1:length([Out.Cycle])]];
        else %added 9.9.2013
            fprintf(['Not enough cycle to remove initiation and termination according to your settings \n'])
            Init=0; % force init to be 0;
            %             web(UrlGaitUp);
            return
        end
    end
else %two feet
    % Classify RL cycles as straight/turning
    %     if find(OutL.Cycle(1).HS==OutRL.Cycle(1,1).HsR)==1 % Left foot initiation
    %         for i=1:min(length(OutRL.Cycle),length(OutR.Cycle))%changed
    %             OutRL.Cycle(i).TA=OutR.Cycle(i).TA; %opposite foot (right) in swing phase > give turning amount
    %         end
    %     else                                            % right foot initiation
    %         for i=1:min(length(OutRL.Cycle),length(OutL.Cycle))
    %             OutRL.Cycle(i).TA=OutL.Cycle(i).TA; %opposite foot (left) in swing phase > give turning amount
    %         end
    %     end
    %     DiscardC.turn=find(abs([OutRL.Cycle.TA])>20); %delete step length values after turn
    %     if length(DiscardC.turn) > 0
    %         startTurn=DiscardC.turn(1);
    %         for i=startTurn:min(length(OutR.Cycle), length(OutL.Cycle))% put NaN values for step length and its asymmetry during and after turn.
    %             OutR.Cycle(i).stepl=NaN;
    %             OutL.Cycle(i).stepl=NaN;
    %             OutL.Cycle(i).stepAS_spat=NaN;
    %         end
    %     end
    if Turn==1
        DiscardC.turns=union(find(abs([OutRL.Cycle.TA_R])>20),find(abs([OutRL.Cycle.TA_L])>20));
    end
    if Init>0
        if length([OutRL.Cycle])>2*Init+1 % verify number of cycle to remove initiation and termination
            DiscardC.init=[[1:Init] [length([OutRL.Cycle])-Init+1:length([OutRL.Cycle])]];
        else
            fprintf(['There is not enough cycle to remove initiation and termination according to your settings \n'])
            Init=0; % force init to be 0
            %             web(UrlGaitUp);
            return
        end
    end
end

%% Save and export results

if Save==1
    % Test parameters
    if SensorN==1
        result.TestTime=length(Out.Signal)/200; %based on raw signal
        
        result.TurnTime=nansum([Out.Cycle([DiscardC.turns]).gct]);
        result.InitTime=nansum([Out.Cycle([DiscardC.init]).gct]);
        result.GaitTime=(Out.Cycle(end).FF2-Out.Cycle(1).FF1+1)/200;
        %result.GaitTime=nansum([Out.Cycle.gct])-result.TurnTime-result.InitTime;
        
        result.NTurnCycles=length([DiscardC.turns]);
        result.NInitCycles=length([DiscardC.init]);
        result.NGaitCycles=length(Out.Cycle);
        %result.NGaitCycles=length(Out.Cycle)-result.NTurnCycles-result.NInitCycles;
        
        
        % STATISTICS
        MET=struct2cell(Out.Cycle);
        MAT=nan(length(MET(:,1,1)),length(MET(1,1,:)));
        for i=1:length(MET(1,1,:))
            if ~isempty(cell2mat(MET(:,1,i)))
                MAT(:,i)=cell2mat(MET(:,1,i));
            end
        end
        MAT(5,:)=MAT(5,:)/200; % Heelstrike time conversion in seconds;
        
        % DISCARD TURNS AND INIT according to options
        Analyse=[1:length(MAT(1,:))];
        if Turn==1
            Analyse=setdiff(Analyse,[DiscardC.turns]);
        end
        if Init>0
            Analyse=setdiff(Analyse,[DiscardC.init]);
        end
        MAT=MAT([5,8:end],Analyse); % added HS
        result.NDiscardedCycles=length(Out.Cycle)-length(Analyse);
        result.AnalysedDistance=nansum(MAT(9,:)); % stride Length is param #9
        Labels=fieldnames(Out.Cycle);
        result.Labels=Labels([5,8:end])';
        
        Statistics=zeros(7,length(MAT(:,1)));
        Statistics(1,:)=nanmean(MAT,2)';
        Statistics(2,:)=nanmedian(MAT,2)';
        Statistics(3,:)=nanstd(MAT,[],2)';
        Statistics(4,:)=iqr(MAT,2)';
        Statistics(5,:)=min(MAT');
        Statistics(6,:)=max(MAT');
        
        % Not Symetry measures with one foot
        
        % CV
        if length(Analyse) > 10 %calculates CV for some parameters if enough steps
            Statistics(7,:)=100*Statistics(3,:)./Statistics(1,:);
        end
        %         if result.NGaitCycles-result.NDiscardedCycles > 10 %if enough gait cycles calculate CV for certain parameters
        %             Statistics(7,:)=nan(1,length(MAT(:,1)));
        %             if Clea>0
        %                 Statistics(7,[1:8, 10:11,13:15])= 100*Statistics(3,[1:8,10:11,13:15])./Statistics(1,[1:8,10:11,13:15]); %CV in%
        %             else
        %                 Statistics(7,[1:8, 10:11])= 100*Statistics(3,[1:8,10:11])./Statistics(1,[1:8,10:11]);
        %             end
        %         end
        
    else % two feet
        result.TestTime=min(length(OutR.Signal)/200,length(OutL.Signal)/200); %based on raw signal
        
        result.GaitTime=(max(OutR.Cycle(end).FF2,OutL.Cycle(end).FF2)-min(OutR.Cycle(1).FF1,OutL.Cycle(1).FF1)+1)/200;
        result.TurnTime=nansum([OutRL.Cycle([DiscardC.turns]).gct]);
        result.InitTime=nansum([OutRL.Cycle([DiscardC.init]).gct]);
        % WARNING: WHY THE VARIABLE 'Out' IS USED IN THE FOLLOWING LINE AND
        % NOT OUTRL?
        %result.GaitTime=nansum([Out.Cycle.gct])-result.TurnTime-result.InitTime;
        
        
        result.NTurnCycles=length([DiscardC.turns]);
        result.NInitCycles=length([DiscardC.init]);
        result.NGaitCycles=length(OutRL.Cycle);
        %result.NGaitCycles=length(OutRL.Cycle)-result.NTurnCycles-result.NInitCycles;
        
        %        result.NRightSteps=length(OutR.Cycle);
        %         result.NLeftSteps=length(OutL.Cycle);
        
        %         if result.NRightSteps>result.NLeftSteps
        %             result.InitTime=nansum([OutR.Cycle([DiscardC.init]).gct]);
        %         else
        %             result.InitTime=nansum([OutL.Cycle([DiscardC.init]).gct]);
        %         end
        
        
        %         % STATISTICS
        %         METR=struct2cell(OutR.Cycle);
        %         MATR=nan(length(METR(:,1,1)),length(METR(1,1,:)));
        %         for i=1:length(METR(1,1,:))
        %             if ~isempty(cell2mat(METR(:,1,i)))
        %                 MATR(:,i)=cell2mat(METR(:,1,i));
        %             end
        %         end
        %         METL=struct2cell(OutL.Cycle);
        %         MATL=nan(length(METL(:,1,1)),length(METL(1,1,:)));
        %         for i=1:length(METL(1,1,:))
        %             if ~isempty(cell2mat(METL(:,1,i)))
        %                 MATL(:,i)=cell2mat(METL(:,1,i));
        %             end
        %         end
        
        % STATISTICS
        MET=struct2cell(OutRL.Cycle);
        MAT=nan(length(MET(:,1,1)),length(MET(1,1,:)));
        for i=1:length(MET(1,1,:))
            if ~isempty(cell2mat(MET(:,1,i)))
                MAT(:,i)=cell2mat(MET(:,1,i));
            end
        end
        MAT(1:5,:)=MAT(1:5,:)/200; % Heelstrike time conversion in seconds;
        
        % DISCARD TURNS AND INIT according to options
        Analyse=[1:length(MAT(1,:))];
        if Turn==1
            Analyse=setdiff(Analyse,[DiscardC.turns]);
        end
        if Init>0
            Analyse=setdiff(Analyse,[DiscardC.init]);
        end
        MAT=MAT([1,4,9:end],Analyse); % Start at DS, added HS, do not export fields FF1, HO, ... to FF2
        
        %         % do not export fields FF1, HO, ... to FF2, and discard cycles if
        %         % needed
        %         AnalyseR=setdiff([1:length(MATR(1,:))],[DiscardC.turns DiscardC.init]);
        %         AnalyseL=setdiff([1:length(MATL(1,:))],[DiscardC.turns DiscardC.init]);
        %
        %         delta = 0;
        %         % FF1_L(2+) > FF1_R(1)
        %         if (length(OutL.Cycle) > 1)
        %             for i=2:length(OutL.Cycle)
        %                 if (OutL.Cycle(i).FF1 > OutR.Cycle(1).FF1)
        %                     break;
        %                 end
        %                 delta = delta - 1;
        %             end
        %         end
        %         % FF1_R(2+) > FF1_L(1)
        %         if (length(OutR.Cycle) > 1)
        %             for i=2:length(OutR.Cycle)
        %                 if (OutR.Cycle(i).FF1 > OutL.Cycle(1).FF1)
        %                     break;
        %                 end
        %                 delta = delta + 1;
        %             end
        %         end
        %
        %         MATR=MATR([8:end],AnalyseR);
        %         MATL=MATL([8:end],AnalyseL);
        %
        %         result.NDiscardedRightSteps=result.NRightSteps-length(AnalyseR);
        %         result.NDiscardedLeftSteps=result.NLeftSteps-length(AnalyseL);
        %
        %         % Check for extra steps for one foot or the other
        %         if (delta == 0)
        %             % put aside right and left parameters
        %             if (length(MATR(1,:)) > length(MATL(1,:)))
        %                 delta = 1;
        %             elseif (length(MATR(1,:)) < length(MATL(1,:)))
        %                 delta = -1;
        %             end
        %         end
        %
        %         % Construct the final table for the cycles
        %         if (delta > 0)
        %             % more right cycles
        %             len = length(MATL(:,1));
        %             MAT=[MATR;[NaN(len,delta) MATL NaN(len,size(MATR,2)-size(MATL,2)-delta)]];
        %         else
        %             % more left cycles
        %             len = length(MATR(:,1));
        %             MAT=[[NaN(len,-delta) MATR NaN(len,size(MATL,2)-size(MATR,2)+delta)];MATL];
        %         end
        
        %         result.NDiscardedCycles=result.NGaitCycles-length(Analyse);
        %         result.AnalysedTime=nansum(MAT(1,:));
        %         result.AnalysedDistance=nansum(MAT(8,:));
        %         Labels=fieldnames(Out.Cycle);
        %         result.Labels=Labels(8:end)';
        %
        %         Statistics=zeros(6,length(MAT(:,1)));
        %         Statistics(1,:)=nanmean(MAT,2)';
        %         Statistics(2,:)=nanmedian(MAT,2)';
        %         Statistics(3,:)=nanstd(MAT,[],2)';
        %         Statistics(4,:)=iqr(MAT,2)';
        %         Statistics(5,:)=min(MAT');
        %         Statistics(6,:)=max(MAT');
        %         if result.NGaitCycles-result.NDiscardedCycles > 10 %if enough gait cycles calculate CV for certain parameters
        %             Statistics(7,:)=nan(1,length(MAT(:,1)));
        %             if Clea>0
        %                 Statistics(7,[1:8, 10:11,13:15])= 100*Statistics(3,[1:8,10:11,13:15])./Statistics(1,[1:8,10:11,13:15]); %CV in%
        %             else
        %                 Statistics(7,[1:8, 10:11])= 100*Statistics(3,[1:8,10:11])./Statistics(1,[1:8,10:11]);
        %             end
        %         end
        
        result.NDiscardedCycles=length(OutRL.Cycle)-length(Analyse);
        result.AnalysedDistance=nansum(MAT(18,:)); % Right Stride Length is #18 with 2 feets
        Labels=fieldnames(OutRL.Cycle);
        result.Labels=Labels([1,4,9:end])';
        
        Statistics=zeros(8,length(MAT(:,1)));
        Statistics(1,:)=nanmean(MAT,2)';
        Statistics(2,:)=nanmedian(MAT,2)';
        Statistics(3,:)=nanstd(MAT,[],2)';
        Statistics(4,:)=iqr(MAT,2)';
        Statistics(5,:)=min(MAT');
        Statistics(6,:)=max(MAT');
        
         % CV
        if length(Analyse) > 1 %calculates CV for some parameters if enough steps
            Statistics(7,:)=100*Statistics(3,:)./Statistics(1,:);
        end
        
        % Symetry Ratio using mean
        for i=4:2:length(MAT(:,1)) % start at gct
            Statistics(8,i)=nanmean(MAT(i,:)./MAT(i+1,:));
            Statistics(8,i+1)=nanmean(MAT(i+1,:)./MAT(i,:));
        end
        
       
        %             if Clea>0
        %                 %Statistics(7,[1:9, 11:12, 14:16, 17:25, 26:27, 28:32, 45:46])= 100*Statistics(3,[1:9, 11:12, 14:16, 22:32, 34:35, 37:39, 45:46])./Statistics(1,[1:9, 11:12, 14:16, 22:32, 34:35, 37:39, 45:46]); %CV in%
        %                 % stat avec step time etc
        %                 % Statistics(7,[1:9, 11:12, 14:16, 22:32, 34:35, 37:39, 45:46])= 100*Statistics(3,[1:9, 11:12, 14:16, 22:32, 34:35, 37:39, 45:46])./Statistics(1,[1:9, 11:12, 14:16, 22:32, 34:35, 37:39, 45:46]); %CV in%
        %             else
        %                 %Statistics(7,[1:9, 11:12, 14:16, 17:25, 26:27, 28:32])= 100*Statistics(3,[1:9, 11:12, 14:16, 17:25, 26:27, 28:32])./Statistics(1,[1:9, 11:12, 14:16, 17:25, 26:27, 28:32]); %CV in%
        %                 %Statistics(7,[1:9, 11:12, 14:16, 22:32, 34:35, 37:39])= 100*Statistics(3,[1:9, 11:12, 14:16, 22:32, 34:35, 37:39])./Statistics(1,[1:9, 11:12, 14:16, 22:32, 34:35, 37:39]);
        %             end
        %         end
    end
    
    %%%%%CHECK RESULTS AND INFORM USER%%%%% added 9.9.2013 by Rebekka
    
    % Check Shoe size
    if Clea < 10 || Clea > 34
        fprintf(['Warning 1 (Shoe size), details can be found on our website: ' UrlGaitUp '\n']);
%         web(UrlGaitUp);
    else
        %Take mean +/- 3*std from data of Benoit's thesis
        %Check stance time (62.04847 +/-3*2.0447714 %), max heel clearance (0.276069 +/-3*0.0197637 m)
        %and stride velocity ( 1.1419895 +/- 3*0.0780287 m/s)
        %         if SensorN ==1
        NGaitCyclesAnalyzed = result.NGaitCycles - result.NDiscardedCycles; %Check if enough gait cycles
        if NGaitCyclesAnalyzed < 4
            fprintf(['Warning 2 (Number of cycles), details can be found on our website: ' UrlGaitUp '\n']);
%             web(UrlGaitUp);
            %3*std: 99.7% of values should be included, p=0.003
            %             elseif Statistics(1,3)< 55.914156 || Statistics(1,3)> 68.182784 || Statistics(1,16)< 0.216778 || Statistics(1,16)> 0.33536 || Statistics(1,11)< 0.9079034 || Statistics(1,11)> 1.3760756
            %                 fprintf(['Warning 3 (Parameter range), details can be found on our website: ' UrlGaitUp '\n']);
            %                 web(UrlGaitUp);
        else
            fprintf('Your data has been sucessfully analyzed \n');
        end
        %         else
        %             NGaitCyclesAnalyzed = result.NGaitCycles - max(result.NDiscardedRightSteps, result.NDiscardedLeftSteps);
        %             if NGaitCyclesAnalyzed < 5
        %                 fprintf(['Warning 2 (Number of cycles), details can be found on our website: ' UrlGaitUp '\n']);
        %                 web(UrlGaitUp); %CHECK POSITION OF VARIABLES (Changed due to add of step time & length -> normally left 2 fields further)
        %             elseif Statistics(1,3)< 55.914156 || Statistics(1,3)> 68.182784 || Statistics(1,17)< 0.216778 || Statistics(1,17)> 0.33536 ||  Statistics(1,12)< 0.9079034 || Statistics(1,12)> 1.3760756 || Statistics(1,26)< 55.914156 || Statistics(1,26)> 68.182784 || Statistics(1,40)< 0.216778 || Statistics(1,40)> 0.33536 || Statistics(1,35)< 0.9079034 || Statistics(1,35)> 1.3760756
        %                 if Statistics(1,3)< 55.914156 || Statistics(1,3)> 68.182784 || Statistics(1,17)< 0.216778 || Statistics(1,17)> 0.33536 ||  Statistics(1,12)< 0.9079034 || Statistics(1,12)> 1.3760756
        %                     fprintf(['Warning 4 (Parameter range, right side), details can be found on our website: ' UrlGaitUp '\n']);
        %                     web(UrlGaitUp);
        %                 end
        %                 if Statistics(1,26)< 55.914156 || Statistics(1,26)> 68.182784 || Statistics(1,40)< 0.216778 || Statistics(1,40)> 0.33536 || Statistics(1,35)< 0.9079034 || Statistics(1,35)> 1.3760756
        %                     fprintf(['Warning 5 (Parameter range, left side), details can be found on our website: ' UrlGaitUp '\n']);
        %                     web(UrlGaitUp);
        %                 end
        %             else
        %                 fprintf('Your data has been sucessfully analyzed \n');
        %             end
        %         end
    end
    result.Statistics=Statistics;
    % result structure To export to excel
    result.Cycles =MAT';
end

if Expo==1
    result.Signal=Signal;
    % result to export to CSV
end

%     Computation of step time


if result.Cycles(1,1) < result.Cycles(1,2)      % heel strike right is first
    step_timeR = result.Cycles(:,1) - result.Cycles(:,2);
    step_timeL = result.Cycles(:,2) - circshift(result.Cycles(:,1),1);
end


end

%% Sub-functions
function Signal4=GaitUp_Calibration(Signal)
% AUTOMATIC CALIBRATION OF SENSOR ORIENTATION ON FOOT

%Signal(:,6)=-Signal(:,6); % Physilog 3 convention: 3rd gyro is inverted >>> TO CHECK on PHYSILOG 4 !!!
Signal(:,5)=-Signal(:,5); % Physilog 4 gyro no 2 inverted compared to P3
Signal1(:,4)=Signal(:,6);
Signal1(:,5)=Signal(:,4);
Signal1(:,6)=Signal(:,5);
Signal(:,1)=-Signal(:,1); %Accelerometer no 1 inverted in comparison to Physilog 3
Signal(:,3)=-Signal(:,3); %Accelerometer no 3 inverted in comparison to Physilog 3
Signal1(:,1)=Signal(:,3);
Signal1(:,2)=Signal(:,1);
Signal1(:,3)=Signal(:,2);
Signal=Signal1;

%calibration correction %%TO CHECK (added 24.2.2014)
% accOffset=header.sensor(1).offset;
% gyroOffset=header.sensor(2).offset;
% accGain=header.sensor(1).sensitivity;
%
% Signal(:,1:3)=(Signal(:,1:3)-accOffset)/accGain;
% Signal(:,4:6)=Signal(:,4:6)-gyroOffset;

GyroNorm=ANorm(Signal(:,4:6));

% Find static periods with adaptative bi-threshold method on gyro's norm
ZUPvth=3;       % value threshold
ZUPdth=5;       % duration threshold
rawZUPR=find(GyroNorm<mode(GyroNorm)+ZUPvth);
ZUPR=[];
for i=ZUPdth+1:length(rawZUPR)
    if rawZUPR(i-ZUPdth)==rawZUPR(i)-ZUPdth
        ZUPR=[ZUPR;rawZUPR(i)];
    end
end

if length(ZUPR)<20 % if less than 20 static points where found, increase thresholds
    while length(ZUPR)<20
        fprintf('>have increased static period threshold of 1\n')
        ZUPvth=ZUPvth+1;  % value threshold
        ZUPdth=5;    % duration threshold
        rawZUPR=find(GyroNorm<mode(GyroNorm)+ZUPvth);
        ZUPR=[];
        for i=ZUPdth+1:length(rawZUPR)
            if rawZUPR(i-ZUPdth)==rawZUPR(i)-ZUPdth
                ZUPR=[ZUPR;rawZUPR(i)];
            end
        end
        
    end
end

% 1st Alignement with gravity vector
[Signal2 MR]=ZeroGMaxAlign(Signal,ZUPR);
if mean(Signal2(ZUPR,3))<0
    angle=180;
    C=cosd(angle);
    S=sind(angle);
    Ry = [C 0 S; 0 1 0; -S 0 C];
    Signal2(:,4:6)=Signal2(:,4:6)*Ry;
    Signal2(:,1:3)=Signal2(:,1:3)*Ry;
end

% 2nd Alignement to find azimuth angle optimizing pitch std
[Signal3 AzimuthR]=GyroAzMaxAlign(Signal2);
Signal4=Signal3;

Pplus=Peaks(Smooth(Signal3(:,5),20),50,75);
Pminus=Peaks(-Smooth(Signal3(:,5),20),50,75);

% 3rd step to find right pitch orientation
if abs(length(Pplus)-length(Pminus))<3 % less than two cycles difference
    % means foot drop => first peak should be negative
    if Pplus(1)<Pminus(1)
        angle=180;
        C=cosd(angle);
        S=sind(angle);
        Rz = [C -S 0; S C 0; 0 0 1];
        Signal4(:,4:6)=Signal3(:,4:6)*Rz;
        Signal4(:,1:3)=Signal3(:,1:3)*Rz;
    end
else
    if length(Pplus)>length(Pminus) % first check number of peaks (not working in case of foot drop)
        angle=180;
        C=cosd(angle);
        S=sind(angle);
        Rz = [C -S 0; S C 0; 0 0 1];
        Signal4(:,4:6)=Signal3(:,4:6)*Rz;
        Signal4(:,1:3)=Signal3(:,1:3)*Rz;
    end
end

% RESET OFFSET (OPTIONNAL)
% Signal4(:,1)=Signal4(:,1)-median(Signal4(:,1));
% Signal4(:,2)=Signal4(:,2)-median(Signal4(:,2));
% Signal4(:,3)=Signal4(:,3)-median(Signal4(:,3))+1;
% Signal4(:,4)=Signal4(:,4)-median(Signal4(:,4));
% Signal4(:,5)=Signal4(:,5)-median(Signal4(:,5));
% Signal4(:,6)=Signal4(:,6)-median(Signal4(:,6));
end

function [Signal1, Signal2] = synchronizer(Signals) %
% Arnaud - 04.04.2014
% Code rewritten to manage the slave and the master based on the header field networkMode.
% The automatic case is discarded for the moment (Not used).
Rchannel1=find([Signals(1).sensor.type]==9,'1');
Rchannel2=find([Signals(2).sensor.type]==9,'1');
% Sensor 1 Master and Sensor 2 Slave
if ((Signals(1).header.networkMode == 2) && (Signals(2).header.networkMode == 1))
    %%% ADD BENOIT 11.09.2014 
    % First Master Packet 
    FMP=Signals(1).sensor(Rchannel1).data(1);
    % Correct Master and slave packets
    Signals(1).sensor(Rchannel1).data=Signals(1).sensor(Rchannel1).data-FMP+1;
    Signals(2).sensor(Rchannel2).data=Signals(2).sensor(Rchannel2).data-FMP+1;
    % Correct Timestamps accordingly
    Signals(1).sensor(Rchannel1).timestamps=Signals(1).sensor(Rchannel1).timestamps-FMP+1;
    Signals(2).sensor(Rchannel2).timestamps=Signals(2).sensor(Rchannel2).timestamps-FMP+1;
    %%%
    
    sample = Signals(2).sensor(Rchannel2).timestamps(1)*200;
    Signal2 = Signals(2).S(ceil(sample):end,:); % (from Signal1 -> Signal2)
    sample = Signals(1).sensor(Rchannel1).timestamps(Signals(2).sensor(Rchannel2).data(1))*200;
    Signal1 = Signals(1).S(ceil(sample):end,:); % (from Signal2 -> Signal1)
    % Sensor 2 Master and Sensor 1 Slave
elseif ((Signals(1).header.networkMode == 1) && (Signals(2).header.networkMode == 2))
    %%% ADD BENOIT 11.09.2014 
    % First Master Packet 
    FMP=Signals(2).sensor(Rchannel2).data(1);
    % Correct Master and slave packets
    Signals(1).sensor(Rchannel1).data=Signals(1).sensor(Rchannel1).data-FMP+1;
    Signals(2).sensor(Rchannel2).data=Signals(2).sensor(Rchannel2).data-FMP+1;
    % Correct Timestamps accordingly
    Signals(1).sensor(Rchannel1).timestamps=Signals(1).sensor(Rchannel1).timestamps-FMP+1;
    Signals(2).sensor(Rchannel2).timestamps=Signals(2).sensor(Rchannel2).timestamps-FMP+1;
    %%%
    
    sample = Signals(1).sensor(Rchannel).timestamps(1)*200;
    Signal1 = Signals(1).S(ceil(sample):end,:);
    sample = Signals(2).sensor(Rchannel).timestamps(Signals(1).sensor(Rchannel).data(1))*200;
    Signal2 = Signals(2).S(ceil(sample):end,:);
else
    % Should not never happen.
    error('Unknown case for the synchronization of the sensor.');
end
% Crop the signal data to have the same number of frames
numFrames = min(size(Signal1,1),size(Signal2,1));
Signal1 = Signal1(1:numFrames,:);
Signal2 = Signal2(1:numFrames,:);
end

function [Out, M] = ZeroGMaxAlign(Signal6D,ZUP)
Out=Signal6D;
SAcc=Signal6D(:,1:3);

%start with a initial orientation of identity
R0 = [0, 0, 0];

%call the nonlinear optimizer to find the rotation matrix
R = fminsearch(@Goal, R0);

%optimization goal function.
    function a = Goal(R)
        ww = rotate3D(SAcc(ZUP,:), R);
        a = abs(1/mean(ww(:,3)));
    end

M=[];

%rotate the signal with R
Out(:,1:3)=rotate3D(Signal6D(:,1:3), R);
Out(:,4:6)=rotate3D(Signal6D(:,4:6), R);

end

function [Out, M] = GyroAzMaxAlign(Signal6D)

SAcc=Signal6D(:,1:3);
SGyro=Signal6D(:,4:6);
%start with a initial orientation of identity
R0 = pi/2;

%call the nonlinear optimizer to find the rotation matrix
R = fminsearch(@Goal, R0);

%optimization goal function.
    function a = Goal(R)
        ww = rotateAz(SGyro, R);
        a = 1/std(ww(:,2));
    end

M=R*180/pi;

%rotate the signal with R
SAccOut(:,1:3)=rotateAz(Signal6D(:,1:3), R);
SGyroOut(:,1:3)=rotateAz(Signal6D(:,4:6), R);
Out=[SAccOut SGyroOut];

end

function b = rotate3D(a, R)
C1=cos(R(1));
S1=sin(R(1));
C2=cos(R(2));
S2=sin(R(2));
C3=cos(R(3));
S3=sin(R(3));
Rx = [1 0 0; 0 C1 -S1; 0 S1 C1];
Ry = [C2 0 S2; 0 1 0; -S2 0 C2];
Rz = [C3 -S3 0; S3 C3 0; 0 0 1];
b = a*Rx*Ry*Rz;
end

function b = rotateAz(a, R)
C=cos(R);
S=sin(R);
Rz = [C -S 0; S C 0; 0 0 1];
b = a*Rz;
end

function Out=GaitUp_Temporal(Signal4)
% AUTOMATIC DETECTION OF TEMPORAL EVENTS

% Detect Global signal features
%-------------------------------
% Global Cadence from spectral analysis on pitch signal
CadenceF=SpectralCadence(Signal4(:,5));
CadenceF=[CadenceF 1]; % in case Spectral candence is empty

% check validity of cadence value or use default
if CadenceF(1)>2 || CadenceF(1)<0.1
    CadenceF(1)=1;
end

% Default Window sized for temporal detections (corresponding to half gait cycle)
window=round(200*(1/CadenceF(1)));

% Check if calibration is correct (Does the gyro need to be flipped of
% 180�?)
MSt=Peaks(Smooth(Signal4(:,5),15),std(Signal4(:,5))/2.5,round(2*window/3));
Strikes=Peaks(Smooth(-Signal4(:,5),15),std(Signal4(:,5))/5,round(window/3));

if length(MSt)>= length(Strikes)            % Number of strikes should be approx double number of midswing. If it is not the case, the algortihm mixed them and signal4 should be flipped
    Signal4(:,5) = -Signal4(:,5);
end

% Midswing detection for forward gait
MSt=Peaks(Smooth(Signal4(:,5),15),std(Signal4(:,5))/2.5,round(2*window/3));

% Strikes detection
Strikes=Peaks(-Signal4(:,5),std(Signal4(:,5))/5,round(window/3));

% Zero crossing detection
ZerosPitch=findZeros(Smooth(Signal4(:,5),10));
ZerosPitch=[ZerosPitch;find(abs(Smooth(Signal4(:,5),10))<5)];
ZerosPitch=sort(ZerosPitch);

% Flat period detection on Gyro (with low and high-selective threshold values)
NormGyro=ANorm(Signal4(:,4:6));
GFlats=Flats2(NormGyro,0.3*std(NormGyro),5);
GFlats2=Flats2(NormGyro,0.5*std(NormGyro),2);

% Associate features to cycles
%-------------------------------
% Recording of temporal event values in Matrix, # of step = # of midswings
Temp=TemporalParameters(MSt,Strikes,ZerosPitch,GFlats,GFlats2,window);

% Finaly recording Motionless instant during footflat periods
MotionLess=FindMotionLess(Temp,Smooth(NormGyro,25));

% Declare events variables
NC=length(MotionLess)-1;
FF1 = zeros(1, NC);
TO = zeros(1, NC);
MS = zeros(1, NC);
HS = zeros(1, NC);
FF2 = zeros(1, NC);
for i=1:NC % Corrected 25/03/2014 by Benoit, 26/03/2014 by Arnaud
    FF1(i)=MotionLess(1,i);
    FF2(i)=MotionLess(1,i+1);
    idx = find((Temp(3,:)>FF1(i)) & (Temp(3,:)<FF2(i)),1);
    if (~isempty(idx))
        TO(i)= Temp(3,idx);
    end
    idx = find((Temp(4,:)>FF1(i)) & (Temp(4,:)<FF2(i)),1);
    if (~isempty(idx))
        MS(i)=Temp(4,idx);
    end
    idx = find((Temp(5,:)>FF1(i)) & (Temp(5,:)<FF2(i)),1);
    if (~isempty(idx))
        HS(i)=Temp(5,idx);
    end
    %Additional test for ensuring order of parameters; added 14.4.14 by
    %Rebekka
    if TO(i)>MS(i) || MS(i)>HS(i)
        TO(i)=0;
    end
end

tc= (TO~=0) & (MS~=0) & (HS~=0); %true cycle corrected 2.4.2014; cycles where event not correctly detected are discarded
FF1=FF1(tc);
FF2=FF2(tc);
TO=TO(tc);
MS=MS(tc);
HS=HS(tc);
NC=length(HS);  %corrected 2.4.2014

% Detect accurate events
%-------------------------
% default values
TS=FF2;
HO=FF1;

% Smoothing Points
Spts=3;

% Signals
Pitch=Smooth(Signal4(:,5),Spts);
LinAcc=Smooth(ANorm(Signal4(:,1:3)),Spts);

% 1st Heel-off:
try
    HO(1)=find(Pitch(FF1(1):TO(1)-10)>(-1*180/pi),1,'last')+FF1(1)-1;
catch
    try
        HO(1)=find(Pitch(FF1(1):TO(1)-5)>(-2*180/pi),1,'last')+FF1(1)-1;
    end
end

% Last Toe-strike:
try
    TS(NC)=find(Pitch(HS(NC)+10:FF2(NC))>(-1*180/pi),1)+HS(NC)+10-1;
catch
    try
        TS(NC)=find(Pitch(HS(NC)+10:FF2(NC))>(-2*180/pi),1)+HS(NC)+10-1;
    end
end

for cy=1:NC-1
    % Heel-strike : Keep original value
    % Toe-strike :
    try
        TS(cy)=find(Pitch(HS(cy)+10:TO(cy+1)-40)>(-1*180/pi),1)+HS(cy)+10-1;
    catch
        try
            TS(cy)=find(Pitch(HS(cy)+10:TO(cy+1)-40)>(-2*180/pi),1)+HS(cy)+10-1;
        end
    end
    % Heel-off :
    try
        HO(cy+1)=find(Pitch(HS(cy)+40:TO(cy+1)-10)>(-1*180/pi),1,'last')+HS(cy)+40-1;
    catch
        try
            HO(cy+1)=find(Pitch(HS(cy)+40:TO(cy+1)-5)>(-2*180/pi),1,'last')+HS(cy)+40-1;
        end
    end
    % Toe-off :
    try
        [maxvalue,maxtime]=max(LinAcc(TO(cy+1)-10:TO(cy+1)+20));
        TO(cy+1)=maxtime+TO(cy+1)-10-1;
    end
end

% Cyclic Parameters
%--------------------
Cycle=[];
Cycle(1).FF1=FF1(1);
Cycle(1).HO=HO(1);
Cycle(1).TO=TO(1);
Cycle(1).MS=MS(1);
Cycle(1).HS=HS(1);
Cycle(1).TS=TS(1);
Cycle(1).FF2=FF2(1);

% Remark: 1st cycle parameters have no values except gct estimated from
% HO1
Cycle(1).gct = (HS(1) - FF1(1)) / 200; % in seconds
Cycle(1).swing = NaN;
Cycle(1).stance = NaN;
Cycle(1).cadence = 120 / Cycle(1).gct;
Cycle(1).LDr=NaN;
Cycle(1).FFr=NaN;
Cycle(1).PUr=NaN;

for i=2:NC
    %%%%%%%%%%%%%%%%%%%%%%% Temporal events %%%%%%%%%%%%%%%%%%%%%
    Cycle(i).FF1=FF1(i);
    Cycle(i).HO=HO(i);
    Cycle(i).TO=TO(i);
    Cycle(i).MS=MS(i);
    Cycle(i).HS=HS(i);
    Cycle(i).TS=TS(i);
    Cycle(i).FF2=FF2(i);
    
    %%%%%%%%%%%%%%%%%%%%%%% Temporal parameters %%%%%%%%%%%%%%%%%%%%%
    Cycle(i).gct = HS(i) - HS(i-1);
    Cycle(i).swing = 100 * (HS(i) - TO(i)) / Cycle(i).gct;
    Cycle(i).stance = 100 * (TO(i) - HS(i-1)) / Cycle(i).gct;
    Cycle(i).gct = Cycle(i).gct / 200; % in seconds
    Cycle(i).cadence = 120 / Cycle(i).gct;
    
    stancetime=TO(i)-HS(i-1);
    Cycle(i).LDr = 100 * (TS(i-1) - HS (i-1))/ stancetime;
    Cycle(i).FFr = 100 * (HO(i) - TS(i-1))/stancetime;
    Cycle(i).PUr = 100 * (TO(i) - HO (i))/stancetime;
end

% Keep signal and parameters for other functions
Out.Signal=Signal4;
Out.Cycle=Cycle;
Out.MotionLess=MotionLess;
Out.Temp=Temp;
end

function Out=spectValue(x,Fs)
if nargin == 1
    Fs = 200;
end
% Use next highest power of 2 greater than or equal to
% length(x) to calculate FFT.
NFFT= 2^(nextpow2(length(x)));

% Take fft, padding with zeros so that length(FFTX) is equal to
% NFFT
FFTX = fft(x,NFFT);

% Calculate the numberof unique points
NumUniquePts = ceil((NFFT+1)/2);

% FFT is symmetric, throw away second half
FFTX = FFTX(1:NumUniquePts);

% Take the magnitude of fft of x
MX = abs(FFTX);

% Scale the fft so that it is not a function of the
% length of x
MX = MX/length(x);

% Take the square of the magnitude of fft of x.
MX = MX.^2;

% Multiply by 2 because you
% threw out second half of FFTX above
MX = MX*2;

% DC Component should be unique.
MX(1) = MX(1)/2;

% Nyquist component should also be unique.
if ~rem(NFFT,2)
    % Here NFFT is even; therefore,Nyquist point is included.
    MX(end) = MX(end)/2;
end

% This is an evenly spaced frequency vector with
% NumUniquePts points.
f = (0:NumUniquePts-1)*Fs/NFFT;

Out=[f;MX'];
end

function [Filtered]=Smooth(Signal,points)

% Low-pass filtering without decay
b = ones(1,points)/points;                           % 20 point averaging filter
Filtered = filtfilt(b,1,Signal);           % Noncausal filtering
end

function Out=Peaks(Signal,Th,zt)

search=1; % search for a max above threshold
Out=[];

while search
    M=max(Signal); % find max value
    if M>Th
        t=find(Signal==M);t=t(1);
        try
            if (~(Signal(t-1)==0 || Signal(t+1)==0))    % Check if peak is not created by zero-ing the signal
                Out=[Out t]; %then store the x coordinate of peak
                Signal(max(t-zt,1):min(t+zt,length(Signal)))=0; % zero signal around peak to avoid double detection
            else
                search =0;
            end
        catch
            Signal(t) = 0.001; % if peak at signal extremities, reduce the max (future max will be adjacent sample)
        end
    else
        search=0;
    end
end

%output vector with times of different peaks in chronological orders
Out=sort(Out);
end

function [Norm]=ANorm(Signal)

if length(Signal(1,:))==2
    Norm=sqrt(Signal(:,1).^2+Signal(:,2).^2);
else
    Norm=sqrt(Signal(:,1).^2+Signal(:,2).^2+Signal(:,3).^2);
end


end

function [Out1 Out2]=MultiStaticFind(Signal,Th,size)
Out1=[];
Out2=[];
j=1;
L=length(Signal);
F=floor(size/4);
while j<L-size
    
    while (mean(Signal(j:j+size),1)+range(Signal(j:j+size)))>Th && j<L-size
        j=j+1;
    end
    if j==L-1
        %     warning('No static period \n')
    else
        i=j+1;
        while (mean(Signal(i:i+F),1)+range(Signal(i:i+F)))<Th && i<L-F-1
            i=i+1;
        end
        
        Out1=[Out1;j];
        Out2=[Out2;i];
        
    end
    j=i+1;
    
end
end

function Out=Flats2(Signal,Th,zoneSize)
Out=[];
[Flats Flatf]=MultiStaticFind(Signal,Th,zoneSize);
for i=1:length(Flats)
    Out=[Out Flats(i):1:Flatf(i)];
end
end

function CadenceF=SpectralCadence(Signal)
sp=spectValue(Signal);
CadenceV=Peaks(sp(2,:),0.2*max(sp(2,:)),10); % Change 25/03/2014: 0.1*max(sp(2,:),7); Change 05/01/2010: 5*std(sp(2,:))
CadenceF=zeros(1,length(CadenceV));
for i=1:length(CadenceV)
    CadenceF(i)=sp(1,CadenceV(i));
end
CadenceF=CadenceF(find(CadenceF>0.4)); % Change 25/03/2014: 0.5->0.4
end

function Temp=TemporalParameters(MS,Strikes,ZerosPitch,GFlats,GFlats2,window)
Temp=zeros(7,length(MS));
for i=1:length(MS)
    % TOE OFFs
    to=find(Strikes<MS(i));
    if numel(to)==0
        t2=find(ZerosPitch<MS(i));
        if numel(t2)==0
            Temp(3,i)=NaN;
        else
            if abs(MS(i)-ZerosPitch(t2(end)))<window/2
                Temp(3,i)=ZerosPitch(t2(end));
            else
                Temp(3,i)=NaN;
            end
        end
    else
        if abs(MS(i)-Strikes(to(end)))<window/2
            Temp(3,i)=Strikes(to(end));
        else
            t2=find(ZerosPitch<MS(i));
            if numel(t2)==0
                Temp(3,i)=NaN;
            else
                if abs(MS(i)-ZerosPitch(t2(end)))<window/2
                    Temp(3,i)=ZerosPitch(t2(end));
                else
                    Temp(3,i)=NaN;
                end
            end
        end
    end
    
    % HEEL STRIKEs
    hs=find(Strikes>MS(i));
    if numel(hs)==0
        h2=find(ZerosPitch>MS(i));
        if numel(h2)==0
            Temp(5,i)=NaN;
        else
            if abs(MS(i)-ZerosPitch(h2(1)))<window/2
                Temp(5,i)=ZerosPitch(h2(1));
            else
                Temp(5,i)=NaN;
            end
        end
    else
        if abs(MS(i)-Strikes(hs(1)))<window/2
            Temp(5,i)=Strikes(hs(1));
        else
            h2=find(ZerosPitch>MS(i));
            if numel(h2)==0
                Temp(5,i)=NaN;
            else
                if abs(MS(i)-ZerosPitch(h2(1)))<window/2
                    Temp(5,i)=ZerosPitch(h2(1));
                else
                    Temp(5,i)=NaN;
                end
            end
        end
        
    end
    
    % MID SWING
    Temp(4,i)=MS(i);
    
    % FOOTFLATs with Th2
    preFF2=find(GFlats2<Temp(3,i));
    if numel(preFF2)==0
        Temp(2,i)=NaN;
    else
        if abs(Temp(3,i)-GFlats2(preFF2(end)))<window/2
            Temp(2,i)=GFlats2(preFF2(end));
        else
            Temp(2,i)=NaN;
        end
    end
    
    postFF2=find(GFlats2>Temp(5,i));
    if numel(postFF2)==0
        Temp(6,i)=NaN;
    else
        if abs(Temp(5,i)-GFlats2(postFF2(1)))<window/2
            Temp(6,i)=GFlats2(postFF2(1));
        else
            Temp(6,i)=NaN;
        end
    end
    
    % FOOTFLATS with Th1
    preFF=find(GFlats<Temp(3,i));
    if numel(preFF)==0
        Temp(1,i)=NaN;
    else
        if abs(Temp(3,i)-GFlats(preFF(end)))<window/2
            Temp(1,i)=GFlats(preFF(end));
        else
            Temp(1,i)=NaN;
        end
    end
    
    postFF=find(GFlats>Temp(5,i));
    if numel(postFF)==0
        Temp(7,i)=NaN;
    else
        if abs(Temp(5,i)-GFlats(postFF(1)))<window/2
            Temp(7,i)=GFlats(postFF(1));
        else
            Temp(7,i)=NaN;
        end
    end
    
    %     if i<length(MS)-1 Test added 26/03/2014 by Rebekka for problematic
    %     file from CERENEO
    %         if MS(i+1)<Temp(7,i)
    %         display('erreur dans ordre parametres temporels');
    %         end
    %     end
end
end

function MotionLess=FindMotionLess(Temp,NormGyro)
PCT=0.4; % Motionless is set at 40% of foot-flat period

if isnan(Temp(1,end))==true && isnan(Temp(7,end))==true
    MotionLess=zeros(1,length(Temp(1,:)));
else
    MotionLess=zeros(1,length(Temp(1,:))+1);
end

if isnan(Temp(1,1))==true
    if isnan(Temp(2,1))==false
        MotionLess(1)=max(Temp(2,1)-40,1);
    end
else
    MotionLess(1)=max(Temp(1,1)-40,1);
end

for i=1:length(Temp(1,:))-1
    if isnan(Temp(1,i+1))==true || isnan(Temp(7,i))==true
        try
            MotionLess(i+1)=floor((Temp(2,i+1)-Temp(6,i))*PCT+Temp(6,i));
        catch
            fprintf('ERROR IN FINDMOTIONLESS\n')
            fprintf(['For support contact your Gait Up team, ' UrlGaitUpContact '\n']);
            web(UrlGaitUpContact);
        end
    else
        MotionLess(i+1)=floor((Temp(1,i+1)-Temp(7,i))*PCT+Temp(7,i));
    end
end

if isnan(Temp(6,end))==true && isnan(Temp(7,end))==true
else
    MotionLess(end)=min(Temp(7,end)+40,length(NormGyro));
end

MotionLess=(setdiff(MotionLess',0,'rows'))';
while isnan(MotionLess(end))
    MotionLess=MotionLess(1:end-1);
end

end

function Out=GaitUp_Spatial(Out)

Signal4=Out.Signal;
Cycle=Out.Cycle;
MotionLess=Out.MotionLess;
Temp=Out.Temp;

% 3D ORIENTATION
%----------------
% Gyroscopic pitch angle
RawPitchAngle=cumtrapz(Signal4(:,5))/200;
[PitchAngle PitchDrift]=LINCor(RawPitchAngle,MotionLess);

% 3D Orientation at Motionless period using accelerometers and Gravity
MATFF=zeros(3,3,length(MotionLess));

MeanSignal1=mean(Signal4(MotionLess(1,:),1));
MeanSignal2=mean(Signal4(MotionLess(1,:),2));
MeanSignal3=mean(Signal4(MotionLess(1,:),3));
NewMat=Inclination([MeanSignal1 MeanSignal2 MeanSignal3]);
MATFF(:,:,1)=NewMat;

for i=2:length(MotionLess)
    MATFF(:,:,i)=NewMat;
end

% Compute Orientation Matrix using quaternions for all signal and tracking of azimuth from two successive cycles
SignalG=-Signal4(:,4:6)*pi/180;
MATp9r=Orientation3D(SignalG,MATFF,MotionLess);

% SENSOR ALIGNEMENT
%--------------------
% Gravity vector
G=[0 0 -1];
% Foot Acceleration in fixed frame without static gravity component
AccF=zeros(length(Signal4),3);
for i=1:length(AccF)
    AccF(i,:)=Signal4(i,1:3)*MATp9r(:,:,i)+G;
end

% 3D Foot Kinematics
%---------------------
% Gravity Value
Gravity=9.81; % >>> can be optimized to geographic position

% Raw Speed in fixed frame from direct integration
RawSpeed=zeros(length(AccF),3);
for i=1:3
    RawSpeed(:,i)=cumtrapz(AccF(:,i))*Gravity/200;
end

% Drift correction with sigmoid method interpolation during motionless
% instants
[Speed SpeedDrift]=SpeedCor(RawSpeed,MotionLess,Temp);

% Raw Distances in fixed frame from direct Integration of Speed
RawDistance=zeros(length(Speed),3);
for i=1:3
    RawDistance(:,i)=cumtrapz(Speed(:,i))/200;
end

% Drift Correction if hypothesis on movement % >>> In ground is flat :
% correct drift on vertical axis
[Distance DistanceDrift]=GroundCor(RawDistance,MotionLess,Temp);
[Distance]=LastCor(Distance,MotionLess);

% Cyclic Parameters
%--------------------
for i=1:length(Cycle)
    
    %%%%%%%%%%%%%%%%%%%%%%% Spatial parameters %%%%%%%%%%%%%%%%%%%%%
    % STRIDE LENGTH
    X1=Distance(Cycle(i).FF1,:);
    X2=Distance(Cycle(i).FF2,:);
    Cycle(i).slength=norm(X2-X1);
    
    % SWING WIDTH (Deviation from 2D Direction)
    V=[1 0 0].*(X2-X1);
    V=V(1)/norm(X2-X1);
    Angle=acosd(V);
    
    if Distance(Cycle(i).FF2,2)-Distance(Cycle(i).FF1,2)>0
        ForwardTraj=rotateZ(Distance(Cycle(i).FF1:Cycle(i).FF2,:)-ones(Cycle(i).FF2-Cycle(i).FF1+1,1)*X1,Angle);
    else
        ForwardTraj=rotateZ(Distance(Cycle(i).FF1:Cycle(i).FF2,:)-ones(Cycle(i).FF2-Cycle(i).FF1+1,1)*X1,-Angle);
    end
    if max(ForwardTraj(:,2))>-min(ForwardTraj(:,2))
        Cycle(i).swidth=max(ForwardTraj(:,2));
    else
        Cycle(i).swidth=min(ForwardTraj(:,2));
    end
    
    % NORMALIZED PATH LENGTH
    x=Distance(Cycle(i).FF1:Cycle(i).FF2,1);
    y=Distance(Cycle(i).FF1:Cycle(i).FF2,2);
    z=Distance(Cycle(i).FF1:Cycle(i).FF2,3);
    Cycle(i).PathLength=100*sum(sqrt((x(2:end)-x(1:end-1)).^2+(y(2:end)-y(1:end-1)).^2+(z(2:end)-z(1:end-1)).^2))/Cycle(i).slength;
    
    % STRIDE VELOCITY
    SpeedNorm=ANorm(Speed(Cycle(i).FF1:Cycle(i).FF2,1:2));
    Cycle(i).speed=mean(SpeedNorm);
    
    % TURNING ANGLE
    Cycle(i).TA=MatAzError(MATp9r(:,:,Cycle(i).FF1),MATp9r(:,:,Cycle(i).FF2));
    
    % ROTATION FEATURES
    Cycle(i).peakswing=max(Signal4(Cycle(i).FF1:Cycle(i).FF2,5));
    Cycle(i).TOP=min(PitchAngle(Cycle(i).FF1:Cycle(i).FF2));
    Cycle(i).HSP=max(PitchAngle(Cycle(i).FF1:Cycle(i).FF2));
end

Out.PitchAngle=PitchAngle;
Out.Cycle=Cycle;
Out.Distance3D=Distance;
Out.Speed3D=Speed;
Out.Orientation3D=MATp9r;
end

function [Mini] = Inclination(Signal)

%start with a initial orientation of identity
R0 = [0, 0, 0];

%call the nonlinear optimizer to find the rotation matrix
R = fminsearch(@GoalI, R0);

%optimization goal function.
    function a = GoalI(R)
        ww = rotate3D(Signal, R);
        a = 1/mean(ww(:,3));
    end
C1=cos(R(1));
C2=cos(R(2));
C3=cos(R(3));
S1=sin(R(1));
S2=sin(R(2));
S3=sin(R(3));

% Compute Initial Orientation Matrix
Rx = [1 0 0; 0 C1 -S1; 0 S1 C1];
Ry = [C2 0 S2; 0 1 0; -S2 0 C2];
Rz = [C3 -S3 0; S3 C3 0; 0 0 1];
Mini=Rx*Ry*Rz;
end

function [MATp9r]=Orientation3D(SignalG,MATFF,MotionLess)
MATp9r=zeros(3,3,length(SignalG));
LastQinit=[1 0 0 0];

%Strapdown integration of gyro at each cycles + azimuth update
for i=1:length(MotionLess)-1
    Qorigin=mat_to_quat(inv(MATFF(:,:,i)));
    Qinit=AzimuthTrack(Qorigin,LastQinit);
    % Qinit=Qorigin;
    Sig=SignalG(MotionLess(1,i):MotionLess(1,i+1),:);
    QUAT=zeros(length(Sig),4);
    QUAT(1,:)=Qinit;
    for l=2:length(Sig)
        QUAT(l,:)=SDI(QUAT(l-1,:)',Sig(l,:)',1/200)';
    end
    %     [QUAT]=gyr_to_quat_S(SignalG(MotionLess(1,i):MotionLess(1,i+1),:),Qinit);
    
    LastQinit=QUAT(end,:);
    for j=1:length(QUAT)
        MATp9r(:,:,j+MotionLess(1,i)-1)=inv(quat_to_mat(QUAT(j,:)));
    end
end

% Initial Orientation
for i=1:MotionLess(1,1)
    MATp9r(:,:,i)=MATp9r(:,:,MotionLess(1,1));
end

% final Orientation
for i=MotionLess(1,end)+1:length(MATp9r)
    MATp9r(:,:,i)=MATp9r(:,:,MotionLess(1,end));
end
end

function [quat] = mat_to_quat(M)
l=length(M(1,1,:));
quat= [ones(l,1),zeros(l,3)];

m=squeeze(M(:,:,1));
trace = m(1,1)+m(2,2)+m(3,3)+1;
s = 0.5 / sqrt(trace);
q(1) = 0.25 / s;
q(2) = ( m(3,2)-m(2,3)) * s;
q(3) = ( m(1,3)-m(3,1)) * s;
q(4) = ( m(2,1)-m(1,2)) * s;
quat(1,:) = q/norm(q);

for k=2:l
    m=squeeze(M(:,:,k));
    trace = m(1,1)+m(2,2)+m(3,3)+1;
    q = [1 0 0 0];
    if trace >0
        s = 0.5 / sqrt(trace);
        q(1) = 0.25 / s;
        q(2) = ( m(3,2)-m(2,3)) * s;
        q(3) = ( m(1,3)-m(3,1)) * s;
        q(4) = ( m(2,1)-m(1,2)) * s;
    elseif ( m(1,1) > m(2,2) && m(1,1) > m(3,3) )
        s = 2 * sqrt( 1.0 + m(1,1)-m(2,2)-m(3,3));
        q(1) = (m(2,3)-m(3,2))/s;
        q(2) = 0.25 * s;
        q(3) = (m(1,2)+m(2,1))/s;
        quat(4) = (m(1,3)+m(3,1))/s;
    elseif m(2,2)>m(3,3)
        s = 2 * sqrt( 1 + m(2,2)-m(1,1)-m(3,3));
        q(1) = (m(1,3)-m(3,1))/s;
        q(2) = (m(1,2)+m(2,1))/s;
        q(3) = 0.25 * s;
        q(4) = (m(2,3)+m(3,2))/s;
    else
        s = 2 * sqrt( 1 + m(3,3)-m(1,1)-m(2,2));
        q(1) = (m(1,2)-m(2,1))/s;
        q(2) = (m(1,3)+m(3,1))/s;
        q(3) = (m(2,3)+m(3,2))/s;
        q(4) = 0.25 * s;
    end
    
    if q(2:4)*quat(k-1,2:4)'<0
        q =-q;
    end
    quat(k,:) = q/norm(q);
end
end

function [mat]=quat_to_mat(q)
l=length(q(:,1));
mat=zeros(3,3,l);
for k=1:l
    R=zeros(3,3);
    R(1,1)=1-2*(q(3)^2+q(4)^2);
    R(1,2)=2*(q(2)*q(3)-q(4)*q(1));
    R(1,3)=2*(q(2)*q(4)+q(3)*q(1));
    
    R(2,1)=2*(q(2)*q(3)+q(4)*q(1));
    R(2,2)=1-2*(q(2)^2+q(4)^2);
    R(2,3)=2*(q(3)*q(4)-q(2)*q(1));
    
    R(3,1)=2*(q(2)*q(4)-q(3)*q(1));
    R(3,2)=2*(q(3)*q(4)+q(2)*q(1));
    R(3,3)=1-2*(q(2)^2+q(3)^2);
    mat(:,:,k)=R;
end
end

function out=MatAzError(RefM,SensorM)

angle0=0; % angle in degree
angle = fminsearch(@GoalQ, angle0);

    function a = GoalQ(angle)
        MATww = rotateZ(SensorM,angle);
        a = abs(MatHelicalAngle(RefM,MATww)); % helicoidal angle between MATref and MATin should be minimum
    end
out=angle;
end

function [Speed SpeedDrift]=SpeedCor(RawSpeed,MotionLess,Temp)
SpeedDrift=zeros(length(RawSpeed),3);
Speed=RawSpeed;
% Speed Correction with fitting of drift curve at footflats
% SigmoidWindow=1; % number of points to be assessed as no movement around motionless

for j=1:3   % loop for each channel
    
    if isnan(Temp(1,1))==false
        ffTemp=1:1:max(1,Temp(1,1)-20);
        ffValue=RawSpeed(1:max(1,Temp(1,1)-20),j)';
    else
        ffTemp=1:1:MotionLess(1,1);
        ffValue=RawSpeed(1:MotionLess(1,1),j)';
    end
    
    for i=2:length(Temp(1,:))
        try
            ffValue=[ffValue RawSpeed(Temp(7,i-1)+min(floor((Temp(1,i)-Temp(7,i-1))/2-1),20):Temp(1,i)-min(floor((Temp(1,i)-Temp(7,i-1))/2-1),20),j)'];
            ffTemp=[ffTemp Temp(7,i-1)+min(floor((Temp(1,i)-Temp(7,i-1))/2-1),20):1:Temp(1,i)-min(floor((Temp(1,i)-Temp(7,i-1))/2-1),20)];
        catch
            try
                ffValue=[ffValue RawSpeed(Temp(6,i-1)+min(floor((Temp(2,i)-Temp(6,i-1))/2-1),30):Temp(2,i)-min(floor((Temp(2,i)-Temp(6,i-1))/2-1),30),j)'];
                ffTemp=[ffTemp Temp(6,i-1)+min(floor((Temp(2,i)-Temp(6,i-1))/2-1),30):1:Temp(2,i)-min(floor((Temp(2,i)-Temp(6,i-1))/2-1),30)];
            end
        end
    end
    % Last value is taken to avoid interp divergence
    if ffTemp(end)~=length(RawSpeed)
        if isnan(Temp(7,end))==false
            ffValue=[ffValue RawSpeed(min(Temp(7,end)+20,length(RawSpeed)):end,j)'];
            ffTemp=[ffTemp min(Temp(7,end)+20,length(RawSpeed)):1:length(RawSpeed)];
        else
            ffValue=[ffValue RawSpeed(MotionLess(1,end):end,j)'];
            ffTemp=[ffTemp MotionLess(1,end):1:length(RawSpeed)];
        end
    end
    [ffTemp,I,J]=unique(ffTemp);
    ffValue=ffValue(I);
    xi=1:1:length(RawSpeed);
    yi = interp1(ffTemp,ffValue,xi,'pchip');
    Speed(:,j)=RawSpeed(:,j)-yi';
    SpeedDrift(:,j)=yi';
end
end

function [Distance DistanceDrift]=GroundCor(RawDistance,MotionLess,Temp)
Distance=RawDistance;
ffTemp=1:1:min(MotionLess(1,1)+10,length(RawDistance));
ffValue=RawDistance(1:min(MotionLess(1,1)+10,length(RawDistance)),3)';

for i=1:length(Temp(1,:))-1
    if isnan(Temp(1,i+1))==true || isnan(Temp(7,i))==true
        try
            ffValue=[ffValue RawDistance(Temp(6,i)+30:Temp(2,i+1)-30,3)'];
            ffTemp=[ffTemp Temp(6,i)+30:1:Temp(2,i+1)-30];
        end
        
    else
        ffValue=[ffValue RawDistance(Temp(7,i)+20:Temp(1,i+1)-20,3)'];
        ffTemp=[ffTemp Temp(7,i)+20:1:Temp(1,i+1)-20];
    end
end

if isnan(Temp(7,end))==true
    try
        ffValue=[ffValue RawDistance(Temp(6,end)+30:length(RawDistance),3)'];
        ffTemp=[ffTemp Temp(6,end)+30:1:length(RawDistance)];
    end
    
    
else
    ffValue=[ffValue RawDistance(Temp(7,end)+20:length(RawDistance),3)'];
    ffTemp=[ffTemp Temp(7,end)+20:1:length(RawDistance)];
end
% Last value is taken to avoid interp divergence
if ffTemp(end)~=length(RawDistance(:,3))
    ffValue=[ffValue RawDistance(end,3)'];
    ffTemp=[ffTemp length(RawDistance)];
end
[ffTemp,I,J]=unique(ffTemp);
ffValue=ffValue(I);
xi=1:1:length(RawDistance);
yi = interp1(ffTemp,ffValue,xi,'pchip');
Distance(:,3)=RawDistance(:,3)-yi';
DistanceDrift=yi';

end

function [Distance]=LastCor(RawDistance,MotionLess)
Distance=RawDistance;

for j=1:3   % loop for each channel
    ffTemp=1:1:MotionLess(end)-2;
    ffValue=zeros(MotionLess(end)-2,1)';
    
    ffValue=[ffValue (RawDistance(MotionLess(end)-1:end,j)-RawDistance(MotionLess(end)-2,j))'];
    ffTemp=[ffTemp MotionLess(end)-1:1:length(RawDistance)];
    
    xi=1:1:length(RawDistance);
    yi = interp1(ffTemp,ffValue,xi,'pchip');
    Distance(:,j)=RawDistance(:,j)-yi';
end

end

function Qsortie=AzimuthTrack(Qin,Qref)
MATref=inv(quat_to_mat(Qref));
MATin=inv(quat_to_mat(Qin));

angle0=0; % angle in degree
angle = fminsearch(@GoalQ, angle0);

    function a = GoalQ(angle)
        MATww = rotateZ(MATin,angle);
        a = abs(MatHelicalAngle(MATref,MATww)); % helicoidal angle between MATref and MATin should be minimum
    end

Msortie=rotateZ(MATin,angle);
Qsortie=mat_to_quat(inv(Msortie));
end

function b = rotateZ(a,angle)
%     Rx = [1 0 0; 0 cos(R(1)) -sin(R(1)); 0 sin(R(1)) cos(R(1))];
%     Ry = [cos(R(2)) 0 sin(R(2)); 0 1 0; -sin(R(2)) 0 cos(R(2))];
C=cosd(angle);
S=sind(angle);
Rz = [C -S 0; S C 0; 0 0 1];
b = a*Rz;
end

function Q2=SDI(Q1,w,ts)
Wx=[0 -w';w -[0 -w(3) w(2); w(3) 0 -w(1); -w(2) w(1) 0]];
% Wx=[[0 -w(3) w(2); w(3) 0 -w(1); -w(2) w(1) 0], w; -w' 0];
% Q2=Q1+ts*0.5*Wx*Q1;
Q2=expm(0.5*ts*Wx)*Q1;
Q2=Q2/norm(Q2);
end

function out=MatHelicalAngle(RefM,SensorM)

% RefM est une matrice 3*3*i
l=size(RefM);
sl=size(l);
if sl(2)==2
    RotM=SensorM(:,:,1)*inv(RefM(:,:,1));
    %conversion to axis angle:
    theta=abs(acosd((RotM(1,1)+RotM(2,2)+RotM(3,3)-1)/2));
    out=theta;
else
    out=zeros(1,length(RefM(1:3,1:3,:)));
    for i=1:length(RefM)
        RotM=SensorM(:,:,i)*inv(RefM(:,:,i));
        %conversion to axis angle:
        theta=abs(acosd((RotM(1,1)+RotM(2,2)+RotM(3,3)-1)/2));
        out(i)=theta;
    end
end
end

function [Sig Drift]=LINCor(RawSig,MotionLess)
Drift=zeros(length(RawSig),3);
Sig=RawSig;
% Speed Correction with fitting of drift curve at footflats
% SigmoidWindow=1; % number of points to be assessed as no movement around motionless
ffTemp=1:1:MotionLess(1,1);
ffValue=RawSig(1:MotionLess(1,1))';

for i=1:length(MotionLess(1,:))
    %             try
    ffValue=[ffValue RawSig(MotionLess(1,i))'];
    ffTemp=[ffTemp MotionLess(1,i)];
    %             end
    
end

% Last value is taken to avoid interp divergence
ffValue=[ffValue RawSig(end)'];
ffTemp=[ffTemp length(RawSig)];

[ffTemp,I,J]=unique(ffTemp);
ffValue=ffValue(I);
xi=1:1:length(RawSig);
yi = interp1(ffTemp,ffValue,xi,'linear');
Sig=RawSig-yi';
Drift=yi';
end

function Out=GaitUp_Clearance(Out,ShoeSize)

Distance=Out.Distance3D;
MotionLess=Out.MotionLess;
Cycle=Out.Cycle;
MATp9r=Out.Orientation3D;

% Initialises trajectories
traj = Distance;    % estimated foot trajectory
trajS = zeros(size(Distance));  % Sensor trajectory
trajH = zeros(size(Distance));  % Heel trajectory
trajT = zeros(size(Distance));  % Toe trajectory
trajHF = zeros(size(Distance));  % Filtred Heel trajectory
trajTF = zeros(size(Distance));  % Filtred Toe trajectory

% Toe Clearance Model
%----------------------
Vectora = nan(1,length(Cycle));
Vectorb = nan(1,length(Cycle));
Vectorc = nan(1,length(Cycle));

U=MATp9r(1,:,:);
W=MATp9r(3,:,:);

for cy=1:length(Cycle)
    Xhs=traj(Cycle(cy).HS,3);
    Yhs=W(:,3,Cycle(cy).HS);
    Zhs=U(:,3,Cycle(cy).HS);
    Xto=traj(Cycle(cy).TO,3);
    Yto=W(:,3,Cycle(cy).TO);
    Zto=U(:,3,Cycle(cy).TO);
    [a b c]=solve('Xhs+b-b*Yhs-a*Zhs','Xto+b-b*Yto+c*Zto','a+c=ShoeSize/100','a,b,c');
    Xabc=eval([a b c]);
    if Xabc>0
        Vectora(cy) = Xabc(1);
        Vectorb(cy) = Xabc(2);
        Vectorc(cy) = Xabc(3);
    end
    clear Xabc
end

a=nanmedian(Vectora);
b=nanmedian(Vectorb);
c=nanmedian(Vectorc);

if ~isnan(a)
    for cy=1:length(Cycle)
        for t=Cycle(cy).FF1:Cycle(cy).FF2
            trajS(t,:)=traj(t,:)+[0 0 b];
            trajH(t,:)=trajS(t,:)-b*W(:,:,t)-a*U(:,:,t);
            trajT(t,:)=trajS(t,:)-b*W(:,:,t)+c*U(:,:,t);
        end
        % correction of toe trajectory
        trajHF=trajH;
        trajTF(Cycle(cy).FF1:Cycle(cy).FF2,3)=BIMCor(trajT(Cycle(cy).FF1:Cycle(cy).FF2,3),[10 Cycle(cy).FF2-Cycle(cy).FF1-10]);
    end
end

% Correction toe traj at 0 before TO, and Heel at O after heel strike
for cy=1:length(Cycle)
    trajHF(Cycle(cy).HS:Cycle(cy).FF2,3)=0;
    trajTF(Cycle(cy).FF1:Cycle(cy).TO,3)=0;
end

% Cyclic Parameters
%-------------------------------------------------------
% Derivative toe traj to find min an max
trajTDiff=diff(Smooth(trajTF(:,3),5));

% Bias obtained from Validation study
TCbias=-0.018;

for i=1:length(Cycle)
    % Default values
    %----------------
    Cycle(i).maxHC=nan;
    Cycle(i).minTC=nan;
    Cycle(i).maxTC1=nan;
    Cycle(i).maxTC2=nan;
    Cycle(i).mtcS=nan;
    
    % Toe clearance
    %---------------
    try
        Zo3=findZeros(trajTDiff(Cycle(i).TO+2:Cycle(i).HS+15))+Cycle(i).TO+2-1;
        Zo3v=trajTF(Zo3,3);
        if length(Zo3)==3
            Cycle(i).maxTC1=Zo3v(1)-TCbias;
            % Negative measure of MinTC are not acceptable
            if max(0,Zo3v(2)-TCbias)==0
                Cycle(i).minTC=NaN;
            else
                Cycle(i).minTC=Zo3v(2)-TCbias;
            end
            Cycle(i).maxTC2=Zo3v(3)-TCbias;
            Cycle(i).mtcS=ANorm(Out.Speed3D(Zo3(2),:));
        end
    catch
        if max(trajTF(Cycle(i).TO:Cycle(i).FF2),3) ~= 0.0
            Cycle(i).maxTC2=max(trajTF(Cycle(i).TO:Cycle(i).FF2,3))-TCbias;
        end
    end
    
    % Heel clearance
    %----------------
    if max(trajHF(Cycle(i).FF1:Cycle(i).HS,3)) ~= 0.0
        Cycle(i).maxHC=max(trajHF(Cycle(i).FF1:Cycle(i).HS,3));
    end
end

Out.Cycle=Cycle;
Out.SensorClearance=trajS(:,3);
Out.HeelClearance=trajHF(:,3);
Out.ToeClearance=trajTF(:,3);

end

function [Sig Drift]=BIMCor(RawSig,FlatPoints)
Drift=zeros(length(RawSig),3);
Sig=RawSig;
% Clearance Correction with fitting of drift curve at footflats
% SigmoidWindow=1; % number of points to be assessed as no movement around
% motionless
ffTemp=1:1:FlatPoints(1);
ffValue=RawSig(1:FlatPoints(1))';

ffValue=[ffValue RawSig(FlatPoints(2):end)'];
ffTemp=[ffTemp FlatPoints(2):1:length(RawSig)];

ffTemp=[ffTemp floor(length(RawSig)/2)];
ffValue=[ffValue -0.02];

[ffTemp,I,J]=unique(ffTemp);
ffValue=ffValue(I);
xi=1:1:length(RawSig);
yi = interp1(ffTemp,ffValue,xi,'pchip');

Sig=RawSig-yi';
Drift=yi';
end

function OutRL=GaitUp_BiTemporal(OutR,OutL)
% Right side is not known but by convention first side to start is
% considered "right" to match Arash's algorithm

HsR=[OutR.Cycle(:).HS];
ToR=[OutR.Cycle(:).TO];

HsL=[OutL.Cycle(:).HS];
ToL=[OutL.Cycle(:).TO];

Cycle = [];
for i=1:length(HsR)-1
    Cycle(i).HsR = HsR(i);
    Cycle(i).ToR = FindFirstAfter(Cycle(i).HsR, ToR);
    if Cycle(i).ToR == HsR(i) || Cycle(i).ToR >= HsR(i+1)
        Cycle(i).ToR = NaN;
    end
    
    Cycle(i).ToL = FindFirstAfter(Cycle(i).HsR, ToL);
    if Cycle(i).ToL > Cycle(i).ToR
        Cycle(i).ToL = NaN;
    end
    
    Cycle(i).HsL = FindFirstAfter(Cycle(i).ToL, HsL);
    if Cycle(i).HsL > Cycle(i).ToR || Cycle(i).HsL == Cycle(i).ToL
        Cycle(i).HsL = NaN;
    end
    
    Cycle(i).end = HsR(i+1);
    if Cycle(i).HsR == Cycle(i).end
        Cycle(i).end = NaN;
    end
end

%%% patch %%%%
if ~isempty(Cycle) && isnan(Cycle(end).ToR) && isnan(Cycle(end).ToL) && isnan(Cycle(end).HsL)
    Cycle(end) = [];
end

%%% filter %%%
k = 1;
while k < length(Cycle)
    if Cycle(k).ToR == Cycle(k+1).ToR;
        Cycle(k) = [];
    else
        k = k + 1;
    end
end

for i=1:length(Cycle)
    Cycle(i).gct = (Cycle(i).end - Cycle(i).HsR);
    Cycle(i).IDS = 100 * (Cycle(i).ToL - Cycle(i).HsR) / Cycle(i).gct;
    Cycle(i).TDS = 100 * (Cycle(i).ToR - Cycle(i).HsL) / Cycle(i).gct;
    Cycle(i).DS = Cycle(i).IDS + Cycle(i).TDS;
    Cycle(i).gct = Cycle(i).gct/200; % conversion in seconds
end

%Step time based on Heel strikes, 24.9.2013 Rebekka
% Cycle(1).Rstept = OutR.Cycle(1).gct; %calculate first step like first stride
% Cycle(1).Lstept = (Cycle(1).HsL-Cycle(1).HsR) / 200; %to have it in seconds
% for i=2:length(Cycle)
%     Cycle(i).Rstept = (Cycle(i).HsR - Cycle(i-1).HsL) / 200;
%     Cycle(i).Lstept = (Cycle(i).HsL - Cycle(i).HsR) / 200;
% end

OutRL.Cycle=Cycle;

end

function [t, i] = FindFirstAfter(x, a)
%   [t, i] = FindFirstAfter(x, a)
% Finds the first elemet of a, bigger than or equal to x
i = 0;
t = NaN;
if isnan(x) | isempty(a)
    return;
elseif a(end) <= x
    return;
else
    k = binarySearch(x, a);
    if k == 0
        i = k;
        return
    end
    for i=k:length(a)
        if a(i) >= x
            t = a(i);
            return;
        end
    end
end
end

function m = binarySearch(x, a)
%       m = binarySearch(x, a)
% Returns the lower bound of index m in the sorted array a where you can find x
n1 = 1;
n2 = length(a);

if a(1) > x
    m = 0;
end

while(n1 < n2)
    m = fix((n1+n2) / 2);
    if m == n1
        if a(n2) < x
            m = n2;
        end
        return
    elseif a(m) > x
        n2 = m;
    elseif a(m) == x
        return
    else
        n1 = m;
    end
end
m = n1;
end

function out=findZeros(Signal,nb)
out=[];
for i=1:length(Signal)-1
    if sign(Signal(i))~=sign(Signal(i+1))
        if sign(Signal(i))==0 || sign(Signal(i+1))==0
            
        else
            if abs(Signal(i))<abs(Signal(i+1))
                out=[out; i];
            else
                out=[out;i+1];
            end
        end
    end
end
out=unique(out);
if ~isempty(out)
    if nargin>1
        out=out(nb);
    end
else
    out=NaN;
end

end

