function GaitUp_Analysis_a(filename,pathname)

% Ask user for file and if segmented
segmented = questdlg('Are the files segmented?','Segmentation','No');

% Ask number of segments
if (strcmp(segmented,'Yes'))
    answer = inputdlg('Enter number of segments','Segment number',1);
    answer = cell2mat(answer);
    Nsegment = str2num(answer);
else
    Nsegment =1;
end
% Result file name
for l = 1:Nsegment
    DefaultName = {strcat('Segment', num2str(l))};
    name{l} = inputdlg('Enter result file name for segment','File name',[1 50], DefaultName,'on');
end

for i=1:6
    if ~isempty(filename{i})
        fprintf(strcat(['Loading: ', filename{i}, '\n']));
        physilog(i).sensor = importcsvfileRTK(strcat(pathname,filename{i}),segmented);
    end
end

defaultShoeSize={num2str(30)};
for l = 1:Nsegment
    % xls file
    %Replace 0 by NaN
    %Declare variables:
    Cycles = [];
    result.Statistics(8,1:33)=NaN;
    result.Labels=NaN;
    result.Statistics=[];
    result.Labels={};
%     result.Statistics(7,1:3)=[];
    
    column = {'mean','med','std','iqr','min','max','ratio','CV',' ','cycles:'}';
    
    % Create data to be analysed
    
    for i=1:length(physilog)
        if ~isempty(filename{i})
            beg = find(physilog(i).sensor(1).segmentation==l,1,'first');
            lim = find(physilog(i).sensor(1).segmentation==l,1,'last');
            if(isempty(beg))
                beg =1;
                lim = length(physilog(i).sensor(1).segmentation);
            end
            Physilog(i).sensor(1).data =  physilog(i).sensor(1).data(beg:lim,:);
            Physilog(i).sensor(2).data =  physilog(i).sensor(2).data(beg:lim,:);
            Physilog(i).sensor(1).timestamps =  physilog(i).sensor(1).timestamps(beg:lim,:);
            Physilog(i).sensor(2).timestamps =  physilog(i).sensor(2).timestamps(beg:lim,:);
            Physilog(i).sensor(1).type =  physilog(i).sensor(1).type;
            Physilog(i).sensor(2).type =  physilog(i).sensor(2).type;
            Physilog(i).sensor(1).Fs =  physilog(i).sensor(1).Fs;
            Physilog(i).sensor(2).Fs =  physilog(i).sensor(2).Fs;
            Physilog(i).sensor(1).typestrings =  physilog(i).sensor(1).typestrings;
            Physilog(i).sensor(2).typestrings =  physilog(i).sensor(2).typestrings;
        end
    end
    % Main
    %Gait analysis
    if ~isempty(filename{1}) && ~isempty(filename{2})
        options={0,0,0,1,0};
        %%%%----Clearance ask for shoe size START----%%%% added 28.01.2016 by Rebekka
        clearance = questdlg(['Do you want to analyse clearance parameters for segment',num2str(l),'?'],'Clearance','No');
        if (strcmp(clearance,'Yes'))
            answer = inputdlg('Please enter the shoe size in cm','Shoe size',[1 30],defaultShoeSize);
            defaultShoeSize=answer;
            answer = cell2mat(answer);
            shoeSize = str2num(answer);
            if length(shoeSize)>1 %if shoeSize entered with comma instead of point for decimals.
               shoeSize = str2num(strcat([num2str(shoeSize(1)),'.',num2str(shoeSize(2))]));
            end
            if shoeSize >20 && shoeSize < 40
                options={0,0,shoeSize,1,0};
            else
                printf('Error: Shoe size out of normal range. Clearance will not be analyzed. Shoe size must be entered in cm.\n');
                options={0,0,0,1,0};
            end
        end
        %%%%----Clearance ask for shoe size END----%%%%
        %         result=GaitUpCore1_2c_csv(options,Physilog(1).sensor,Physilog(2).sensor);
        result=GaitUpCore_csv(options,Physilog(1).sensor,Physilog(2).sensor);
        % result.Statistics = [result.Statistics,zeros(8,32)];
        % result.Statistics = [result.Statistics; zeros((length(result.Cycles)+3),length(result.Statistics))];
        result.Statistics(1:8,1:2)=NaN(8,2); %delete stat values for Heel strike times bcse no sense
        % Step times & Step lengths
        if result.Cycles(1,1)< result.Cycles(1,2) %start with right side
            stepL = result.Cycles(:,2)-result.Cycles(:,1);
            t = circshift(result.Cycles(:,1),-1);
            stepR = t(1:end-1)-result.Cycles(1:end-1,2); %could use first gct as estimate
            stepLengthL = result.Cycles(:,25).*stepL; %speed*stepTime
            stepLengthR = [result.Cycles(1,18);result.Cycles(2:end,24).*stepR]; %take slength for first step and then speed*stepTime
        else
            stepR = result.Cycles(:,1)-result.Cycles(:,2);
            t = circshift(result.Cycles(:,2),-1);
            stepL = t(1:end-1)-result.Cycles(1:end-1,1);
            stepLengthR = result.Cycles(:,24).*stepR;
            stepLengthL = [result.Cycles(1,19);result.Cycles(2:end,25).*stepL];
        end
        result.Statistics = [result.Statistics,statistics(stepR)',statistics(stepL)',statistics(stepLengthR)',statistics(stepLengthL)'];
        
        if length(stepR) < size(result.Cycles,1)
            stepR = [stepR;nan(size(result.Cycles,1)-length(stepR),1)];
            stepLengthR = [stepLengthR;nan(size(result.Cycles,1)-length(stepLengthR),1)];
        end
        if length(stepL) < size(result.Cycles,1)
            stepL = [stepL;nan(size(result.Cycles,1)-length(stepL),1)];
            stepLengthL = [stepLengthL;nan(size(result.Cycles,1)-length(stepLengthL),1)];
        end
        result.Cycles = [result.Cycles,stepR,stepL, stepLengthR, stepLengthL];
        labelsEnd = length(result.Labels);
        result.Labels(1, labelsEnd+1:labelsEnd+4) = {'StepTime_R','StepTime_L','StepLength_R', 'StepLength_L'};
        
        Cycles = [Cycles, result.Cycles];
%         xlswrite([char(name{l}) '.xls'],result.Cycles,strcat('Sheet', num2str(l)),'B11');
        
    end
    % Arm
    if ~isempty(filename{5}) && ~isempty(filename{6})
        fprintf('Arm analysis \n');
        StatEnd = length(result.Statistics);
        result.Statistics(:,StatEnd+1:StatEnd+2) = NaN(8,2);
        StatEnd = StatEnd+2;
        [result.Statistics(:,StatEnd+1),result.Statistics(:,StatEnd+2),result.Statistics(:,StatEnd+3),...
            result.Statistics(:,StatEnd+4),result.Statistics(:,StatEnd+5),result.Statistics(:,StatEnd+6),result.Statistics(:,StatEnd+7),...
            result.Statistics(:,StatEnd+8),arm_cy,ArmBeg1,ArmBeg2] = GaitUp_arm( Physilog(5).sensor(2).data,Physilog(6).sensor(2).data...
            ,result.Cycles(:,1:2));
        labelsEnd = length(result.Labels);
        result.Labels(1,labelsEnd+1:labelsEnd+10) = {'armCstartT_L', 'armCstartT_R','RoM_L','RoM_R','armCT_L','armCT_R','PCIarm','PCIal_Left','PCIal_Right','PCIleg'};
        if size(Cycles,1)~= max(size(arm_cy,1), max(size(ArmBeg1,1),size(ArmBeg2,1)))
            startI=size(Cycles,1)+1;
            endI=max(size(arm_cy,1), max(size(ArmBeg1,1),size(ArmBeg2,1)));
            Cycles(startI:endI,:)=NaN;
            if size(arm_cy,1)< endI
                arm_cy(end+1:endI,:)=NaN;
            end
            if size(ArmBeg1,1)< endI
                ArmBeg1(end+1:endI,:)=NaN;
            end
            if size(ArmBeg2,1)< endI
                ArmBeg2(end+1:endI,:)=NaN;
            end
            clear endI startI ind
        end
        Cycles = [Cycles, ArmBeg1, ArmBeg2, arm_cy];
        fprintf('Arm analysis completed \n\n');
%         xlswrite([char(name{l}) '.xls'],arm_cy,strcat('Sheet', num2str(l)),'AM11');
%         xlswrite([char(name{l}) '.xls'],ArmBeg1,strcat('Sheet', num2str(l)),strcat('AO', num2str(10+size(arm_cy,1))));
%         xlswrite([char(name{l}) '.xls'],ArmBeg2,strcat('Sheet', num2str(l)),strcat('AP', num2str(10+size(arm_cy,1))));
    end
    
    % Sway
    if ~isempty(filename{4})
        fprintf('Sway analysis \n');
        StatEnd = length(result.Statistics);
        [result.Statistics(1,StatEnd+1),result.Statistics(1,StatEnd+2)] = GaitUp_jerk(Physilog(4).sensor(1).data);
        [result.Statistics(2:8,StatEnd+1:StatEnd+2)]=NaN(7,2);
        Cycles = [Cycles, NaN(size(Cycles,1),2)];
        labelsEnd = length(result.Labels);
        result.Labels(labelsEnd+1:labelsEnd+2) = {'Jerk','Njerk'};
        
        fprintf('Sway analysis completed \n\n');
    end
    
    % Trunk
    if ~isempty(filename{3})
        fprintf('Trunk analysis \n');
        StatEnd = length(result.Statistics);
        [result.Statistics(:,StatEnd+1:StatEnd+24),result.Labels(:,StatEnd+1:StatEnd+24),tr_cy] = GaitUp_trunk (Physilog(3).sensor(2).data,result.Cycles(:,1:2));
        fprintf('Trunk analysis completed \n\n');
        if size(Cycles,1)< size(tr_cy,1)
            startI=size(Cycles,1)+1;
            endI=size(tr_cy,1);
            Cycles(startI:endI,:)=NaN;
        elseif size(Cycles,1)> size(tr_cy,1)
            startI=size(tr_cy,1)+1;
            endI=size(Cycles,1);
            tr_cy(startI:endI,:)=NaN;
        end
        Cycles = [Cycles,tr_cy];
%         xlswrite([char(name{l}) '.xls'],tr_cy,strcat('Sheet', num2str(l)),'AW11');
    end
    
    xlswrite([pathname '/' char(name{l}) '.xls'],result.Labels,strcat('Sheet', num2str(l)),'B1');
    xlswrite([pathname '/' char(name{l}) '.xls'],result.Statistics,strcat('Sheet', num2str(l)),'B2');
    try    xlswrite([pathname '/' char(name{l}) '.xls'],Cycles,strcat('Sheet', num2str(l)),'B11'); end
    xlswrite([pathname '/' char(name{l}) '.xls'],column,strcat('Sheet', num2str(l)),'A2');
    fprintf('Excel file created \n');
end
fprintf('Your analysis is complete\n\n');

end




