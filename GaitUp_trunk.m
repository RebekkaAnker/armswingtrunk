function [ trunk,label,cy ] = GaitUp_trunk( data, cycle )
%GAITUP_TRUNK outputs the mean and max per gait cycle of the yaw,pitch, and
%roll angular velocity and angle as well as symmetry measures
%   Angular velocities are averaged per gait cycle directly (no post
%   processing of the gyroscope signal. Angles are computed using numerical
%   integration of the angular velocity.Symmetry formula compare mean and
%   max per cycle taking alternatively the gait cycles on the right leg and
%   gait cycles on the left leg

cycle = round(cycle.*200);     % Convert cycles times to sample number

Yaw=cumtrapz(data(:,1))/200;      % numerical integration
Pitch=cumtrapz(data(:,2))/200;      % numerical integration
Roll=cumtrapz(data(:,3))/200;      % numerical integration

% [b,a]= butter(3,1/200,'high');        % high pass filtering to correct for drift
% Yaw = filtfilt(b,a,Yaw);
% Pitch = filtfilt(b,a,Pitch);
% Roll = filtfilt(b,a,Roll);

for i=1:length(cycle)-1
    if ~isnan(cycle(i,1)) && ~isnan(cycle(i,2)) && ~isnan(cycle(i+1,1)) && ~isnan(cycle(i+1,2))
        wYawR(i,1) = mean(data(cycle(i,1):cycle(i+1,1),1));       % Average yaw w for right leg cycle
        wYawR(i,2) = max(data(cycle(i,1):cycle(i+1,1),1));        % Max yaw w for right leg cycle
        wYawL(i,1) = mean(data(cycle(i,2):cycle(i+1,2),1));       % Average yaw w for left leg cycle
        wYawL(i,2) = max(data(cycle(i,2):cycle(i+1,2),1));        % Max yaw w for left leg cycle
        
        wRollR(i,1) = mean(data(cycle(i,1):cycle(i+1,1),3));       % Average roll w for right leg cycle
        wRollR(i,2) = max(data(cycle(i,1):cycle(i+1,1),3));        % Max roll w for right leg cycle
        wRollL(i,1) = mean(data(cycle(i,2):cycle(i+1,2),3));       % Average roll w for left leg cycle
        wRollL(i,2) = max(data(cycle(i,2):cycle(i+1,2),3));        % Max roll w for left leg cycle
        
        wPitchR(i,1) = mean(data(cycle(i,1):cycle(i+1,1),2));       % Average pitch w for right leg cycle
        wPitchR(i,2) = max(data(cycle(i,1):cycle(i+1,1),2));        % Max pitch w for right leg cycle
        wPitchL(i,1) = mean(data(cycle(i,2):cycle(i+1,2),2));       % Average pitch w for left leg cycle
        wPitchL(i,2) = max(data(cycle(i,2):cycle(i+1,2),2));        % Max pitch w for left leg cycle
        
        aYawR(i,1) = mean(Yaw(cycle(i,1):cycle(i+1,1)));       % Average yaw w for right leg cycle
        aYawR(i,2) = max(Yaw(cycle(i,1):cycle(i+1,1)));        % Max yaw w for right leg cycle
        aYawL(i,1) = mean(Yaw(cycle(i,2):cycle(i+1,2)));       % Average yaw w for left leg cycle
        aYawL(i,2) = max(Yaw(cycle(i,2):cycle(i+1,2)));        % Max yaw w for left leg cycle
        
        aRollR(i,1) = mean(Roll(cycle(i,1):cycle(i+1,1)));       % Average roll w for right leg cycle
        aRollR(i,2) = max(Roll(cycle(i,1):cycle(i+1,1)));        % Max roll w for right leg cycle
        aRollL(i,1) = mean(Roll(cycle(i,2):cycle(i+1,2)));       % Average roll w for left leg cycle
        aRollL(i,2) = max(Roll(cycle(i,2):cycle(i+1,2)));        % Max roll w for left leg cycle
        
        aPitchR(i,1) = mean(Pitch(cycle(i,1):cycle(i+1,1)));       % Average pitch w for right leg cycle
        aPitchR(i,2) = max(Pitch(cycle(i,1):cycle(i+1,1)));        % Max pitch w for right leg cycle
        aPitchL(i,1) = mean(Pitch(cycle(i,2):cycle(i+1,2)));       % Average pitch w for left leg cycle
        aPitchL(i,2) = max(Pitch(cycle(i,2):cycle(i+1,2)));        % Max pitch w for left leg cycle
        
    else
        wYawR(i,1) = NaN;       % Average yaw w for right leg cycle
        wYawR(i,2) = NaN;        % Max yaw w for right leg cycle
        wYawL(i,1) = NaN;       % Average yaw w for left leg cycle
        wYawL(i,2) = NaN;        % Max yaw w for left leg cycle
        
        wRollR(i,1) = NaN;       % Average roll w for right leg cycle
        wRollR(i,2) = NaN;        % Max roll w for right leg cycle
        wRollL(i,1) = NaN;       % Average roll w for left leg cycle
        wRollL(i,2) = NaN;        % Max roll w for left leg cycle
        
        wPitchR(i,1) = NaN;       % Average pitch w for right leg cycle
        wPitchR(i,2) = NaN;        % Max pitch w for right leg cycle
        wPitchL(i,1) = NaN;       % Average pitch w for left leg cycle
        wPitchL(i,2) = NaN;        % Max pitch w for left leg cycle
        
        aYawR(i,1) = NaN;       % Average yaw w for right leg cycle
        aYawR(i,2) = NaN;        % Max yaw w for right leg cycle
        aYawL(i,1) = NaN;       % Average yaw w for left leg cycle
        aYawL(i,2) = NaN;        % Max yaw w for left leg cycle
        
        aRollR(i,1) = NaN;       % Average roll w for right leg cycle
        aRollR(i,2) = NaN;        % Max roll w for right leg cycle
        aRollL(i,1) = NaN;       % Average roll w for left leg cycle
        aRollL(i,2) = NaN;        % Max roll w for left leg cycle
        
        aPitchR(i,1) = NaN;       % Average pitch w for right leg cycle
        aPitchR(i,2) = NaN;        % Max pitch w for right leg cycle
        aPitchL(i,1) = NaN;       % Average pitch w for left leg cycle
        aPitchL(i,2) = NaN;        % Max pitch w for left leg cycle
        
    end
    
    trunk = zeros(8,24);
    trunk(:,1:4) = trunk_stat(wYawR,wYawL);
    trunk(:,5:8) = trunk_stat(wRollR,wRollL);
    trunk(:,9:12) = trunk_stat(wPitchR,wPitchL);
    trunk(:,13:16) = trunk_stat(aYawR,aYawL);
    trunk(:,17:20) = trunk_stat(aRollR,aRollL);
    trunk(:,21:24) = trunk_stat(aPitchR,aPitchL);
    
    label = {'w_Yaw_R_mean','w_Yaw_R_max','w_Yaw_L_mean','w_Yaw_L_max','w_Roll_R_mean',...
        'w_Roll_R_max','w_Roll_L_mean','w_Roll_L_max','w_Pitch_R_mean','w_Pitch_R_max',...
        'w_Pitch_L_mean','w_Pitch_L_max','a_Yaw_R_mean','a_Yaw_R_max','a_Yaw_L_mean',...
        'a_Yaw_L_max','a_Roll_R_mean','a_Roll_R_max','a_Roll_L_mean','a_Roll_L_max',...
        'a_Pitch_R_mean','a_Pitch_R_max','a_Pitch_L_mean','a_Pitch_L_max'};
    
    cy = [wYawR,wYawL,wRollR,wRollL,wPitchR,wPitchL,aYawR,aYawL,aRollR,aRollL,...
        aPitchR,aPitchL];
end

