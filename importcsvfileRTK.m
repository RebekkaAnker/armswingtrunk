function sensor = importcsvfileRTK(file,segmented)
%Import Physilog 4 data from the csv provided by RTK v1.1 and upwards as a structure.
%New version which reads data more flexibly (order of sensors in csv not
%necessary to be fix)
%   Author: Sylvain Hirth, Rebekka Anker
%
%   Inputs::
%       'file'   - user specified fullfile(path,filename)
%       segmented - 'Yes' or 'No' (question aksed to the user)
%       N - number of segment per file
%
%   Outputs::
%       sensor - struct with fields:
%               Fs - Sampling frequency
%               data   - a matrix containing the 3D values of all
%                        imported data
%               timestamp   - a vector containing the timestamps
%               typestrings  - name of sensor in string format
%               type - type of sensor in number code
%
sensor(1).Fs = 200;
sensor(1).typestrings = 'Accel';
sensor(1).type = 1;
sensor(2).Fs = 200;
sensor(2).typestrings = 'Gyro';
sensor(2).type = 2;
sensor(3).Fs = 40;
sensor(3).typestrings = 'Mag';
sensor(3).type = 3;
sensor(4).Fs = 25;
sensor(4).typestrings = 'Pressure';
sensor(4).type = 4;
sensor(5).Fs = 200;
sensor(5).typestrings = 'None';
sensor(5).data = [];
sensor(5).type = 0;
sensor(5).timestamps = [];
sensor(6).Fs = 200;
sensor(6).typestrings = 'SynchroIn';
sensor(6).type = 10;
sensor(7).Fs = 200/3;
sensor(7).data = [];
sensor(7).timestamps = [];
sensor(7).typestrings = 'None';
sensor(7).type = 0;
sensor(8).Fs = 1;
sensor(8).data = [];
sensor(8).timestamps = [];
sensor(8).typestrings = 'Radio';
sensor(8).type = 9;

%-----Adaptive lecture of sensors independent of order in csv----%
%Read sensors present in file
fileID = fopen(file);
sensors = textscan(fileID, '%[^\n\r]', 7, 'ReturnOnError', false);
fclose(fileID);
%Get sensor type in order from csv
sensors_names = strsplit(sensors{1,1}{4,1},{','});          %for some files from RTK the empty line between header and sensor name does not exist -> sensor names on line 4
if isempty(sensors_names{1,1})
    sensors_names = strsplit(sensors{1,1}{5,1},{','});      %otherwise on line 5
end
%Remove Whitespaces if present at beginning of sensor name
for i=1:length(sensors_names)
    sensors_names{1,i} = strtrim(sensors_names{1,i});
end
clear sensors fileID

%Read data
temp = csvread(file,7,0);
sensors_names = regexprep(sensors_names, '\d', '');
j=1;
for i=1:length(sensors_names)
    switch sensors_names{i}
        case 'Synchro [ hz]'
            sensor(6).timestamps = temp(:,j);
            sensor(6).data = temp(:,j+1);
            sensor(6).segmentation = temp(:,j+2);
            j=j+3;
        case 'Accel [ hz]'
            %sensor(1).typeString = 'Accel';
            sensor(1).timestamps = temp(:,j);
            sensor(1).data = temp(:,j+1:j+3);
            sensor(1).segmentation = temp(:,j+4);
            j=j+5;
        case 'Gyro [ hz]'
            %sensor(2).typeString = 'Gyro';
            sensor(2).timestamps = temp(:,j);
            sensor(2).data = temp(:,j+1:j+3);
            sensor(2).segmentation = temp(:,j+4);
            j=j+5;
        case 'Baro [ hz]'
            %sensor(4).typeString = 'Baro';
            sensor(4).timestamps = temp(:,j);
            sensor(4).data = temp(:,j+1:j+2);
            sensor(4).segmentation = temp(:,j+3);
            j=j+4;
        case 'Magneto [ hz]'
            %sensor(3).typeString = 'Magneto';
            sensor(3).timestamps = temp(:,j);
            sensor(3).data = temp(:,j+1:j+3);
            sensor(3).segmentation = temp(:,j+4);
            j=j+5;
        % For older versions of RTK the sensor channel names were different: 
        case 'Synchro In [ hz]'
            sensor(6).timestamps = temp(:,j);
            sensor(6).data = temp(:,j+1);
            sensor(6).segmentation = temp(:,j+2);
            j=j+3;
        case 'Accelerometer [ hz]'
            %sensor(1).typeString = 'Accel';
            sensor(1).timestamps = temp(:,j);
            sensor(1).data = temp(:,j+1:j+3);
            sensor(1).segmentation = temp(:,j+4);
            j=j+5;
        case 'Gyroscope [ hz]'
            %sensor(2).typeString = 'Gyro';
            sensor(2).timestamps = temp(:,j);
            sensor(2).data = temp(:,j+1:j+3);
            sensor(2).segmentation = temp(:,j+4);
            j=j+5;
        case 'Pression [ hz]'
            %sensor(4).typeString = 'Baro';
            sensor(4).timestamps = temp(:,j);
            sensor(4).data = temp(:,j+1:j+2);
            sensor(4).segmentation = temp(:,j+3);
            j=j+4;
        case 'Magnetometer [ hz]'
            %sensor(3).typeString = 'Magneto';
            sensor(3).timestamps = temp(:,j);
            sensor(3).data = temp(:,j+1:j+3);
            sensor(3).segmentation = temp(:,j+4);
            j=j+5;
    end
end

% Delete last sample if acc or gyro is missing
if(isequal(sensor(1).data(end,:),[0 0 0])|| isequal(sensor(2).data(end,:),[0 0 0]))
    sensor(1).timestamps(end,:) = [];
    sensor(2).timestamps(end,:) = [];
    sensor(1).data(end,:)= [];
    sensor(2).data(end,:)= [];
end

if strcmp(segmented,'Cancel')
    error('Segmentation was not specified')
end
% if no segmentation set all values of segmentation of first channel to 1 
% used as criteria in GaitUp_Analysis_a)
if strcmp(segmented,'No')
    sensor(1).segmentation(:) = 1;
end
    

%%% Segmented file
% if strcmp(segmented,'Yes')
%     % Determine number of lines in csv
%     if (isunix) %# Linux, mac
%         [status, result] = system( ['wc -l ', file] );
%         numlines = str2num(result);
%     elseif (ispc) %# Windows
%         numlines = str2num( perl('countlines.pl', file) );
%     else
%         error('Countlines failed');
%     end
%
%     j=0;
%     fileIDheader = fopen(file,'r'); % open file
%     textscan(fileIDheader, '%[^\n\r]', 7, 'ReturnOnError', false); % read the first 7 lines but don't save
%
%     for i=1:numlines-8
%         line = textscan(fileIDheader,'%f %f %f %f %s %f %f %f %f %s %f %f %f %f %s %f %f %f %s %s %s %s',...
%                         1,'Delimiter',','); % read the line
%         if~(strcmp(line{1,5},'0')) % Note: this assumes that accelerometer and gyro are both labeled together
%             j=j+1;
%             sensor(1).timestamps(j,:) = [line{1,1}];
%             sensor(1).data(j,:) = [line{1,2:4}];
%             sensor(2).timestamps(j,:) = [line{1,6}];
%             sensor(2).data(j,:) = [line{1,7:9}];
%             try
%             sensor(1).segmentation(j,1) = str2double(cell2mat([line{1,5}]));
% %                 sensor(1).segmentation(j,1) = [line{1,5}];
%             catch
%                 disp('Segmentation conversion error');
%             end
%         end
%     end
%
%     fclose(fileIDheader);
%
%     % Delete last sample if acc or gyro is missing
%     if(isequal(sensor(1).data(end,:),[0 0 0])|| isequal(sensor(2).data(end,:),[0 0 0])) %TODO: check condition
%         sensor(1).timestamps(end,:) = [];
%         sensor(2).timestamps(end,:) = [];
%         sensor(1).data(end,:)= [];
%         sensor(2).data(end,:)= [];
%         sensor(1).segmentation(end,:) = [];
%     end



% sensor(1).timestamps(end,:) = [];
% sensor(2).timestamps(end,:) = [];
% sensor(1).data(end,:)= [];
% sensor(2).data(end,:)= [];
% sensor(1).segmentation(end,:) = [];
% end
end