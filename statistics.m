function [ stat ] = statistics( data )
%STATISTICS return some stats about the data vector

stat(:,1) = nanmean(data);
stat(:,2) = nanmedian(data);
stat(:,3) = nanstd(data);
stat(:,4) = iqr(data);
stat(:,5) = min(data);
stat(:,6) = max(data);
stat(:,8) = nanstd(data)/nanmean(data)*100;
stat(:,7)=NaN;        %(format)
end

