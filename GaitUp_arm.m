function [RoMCycle1,RoMCycle2,ArmCycleTime1,ArmCycleTime2,PCIarm,PCIal_Left,PCIal_Right,PCIleg,cy,ArmBeg1,ArmBeg2] = GaitUp_arm( arm1,arm2,cycles )
%GAITUP_ARM compute the range of motion of the writst around the principle
%arm axis, the arm cycle time, the phase coordination index
%   Principal Component Analysis is used to compute the principle arm axis.
%   The gyroscope is rotated accordingly. The angular velocity is
%   numerically integrated to obtain the angle. The angle is subsequently
%   high pass filtered in order to correct for drift.
% 
%       INPUTS: - arm1: [Nx3] vector with the gyroscope data of the left
%                        arm
%               - arm2: [Nx3] vector with the gyroscope data of the right
%                        arm
%               - cycles: gait cycle in seconds 
% 
%       OUTPUTS: - ARoM: Absolute range of motion over the whole recording
%       (in degrees) [Absolute range, Maximum angle forward, Maximum angle
%       backwad]
%               - RoMCycle: Range of motion over the arm cycles
%               - ArmCycleTime: Time between two successive maximum forward
%               angles
%               - PCIarm: Phase coordination index between the two arms
%               - PCIal: Phase coordination index between the arms and legs
%               - cy : cycle values for RoM, Arm cycle time, phi values
%               - ArmBeg : Beginning of cycle times (in seconds)
%       The outputs RoMCycle and ArmCycleTime are stats values of each arm
%       cycle under the form: [mean, meadian, std, iqr, min, max, CV]

R = pca(arm1);      % Principle component analysis
for i=1:length(arm1)        % Rotate the data so that principal rotation is around X (axis 1)
    arm1(i,:) = -(R'*arm1(i,:)')';
end

R = pca(arm2);      % Principle component analysis
for i=1:length(arm2)        % Rotate the data so that principal rotation is around X (axis 1)
    arm2(i,:) = (R'*arm2(i,:)')';
end

Angle1=cumtrapz(arm1(:,1))/200;      % numerical integration
Angle2=cumtrapz(arm2(:,1))/200;      % numerical integration

% [b,a]= butter(3,0.01/200,'high');        % high pass filtering to correct for drift
% Angle = filtfilt(b,a,Angle);

% Arm cycle time
[~,locs1] = findpeaks(Angle1,'MINPEAKDISTANCE',120);        % find peaks separated by 0.6s minimum
for i=1:length(locs1)-1
    act1(i) = locs1(i+1)-locs1(i);
end
ArmCycleTime1 = statistics(act1'./200);
ArmBeg1 = locs1./200;

[~,locs2] = findpeaks(Angle2,'MINPEAKDISTANCE',120);        % find peaks separated by 0.6s minimum
for i=1:length(locs2)-1
    act2(i) = locs2(i+1)-locs2(i);
end
ArmCycleTime2 = statistics(act2'./200);
ArmBeg2 = locs2./200;

for i=1:length(locs2)-1
    deb = find(locs1>locs2(i) & locs1<locs2(i+1));
    if length(deb)>1
       deb = deb(2);
    end
       
    if ~isempty(deb) && deb < min(length(act1),length(act2))
       ratio(i) = (act1(deb)-act2(i))/(act2(deb)+act1(i));
    else
       ratio(i) = NaN;        
    end
end
Symm(:,1) = nanmean(ratio);
clear ratio

for i=1:length(locs1)-1
    deb = find(locs2>locs1(i) & locs2<locs1(i+1));
    if length(deb)>1
       deb = deb(2);
    end
    
    if ~isempty(deb) && deb < min(length(act1),length(act2))
       ratio(i) = (act2(deb)-act1(i))/(act2(deb)+act1(i));
    else
       ratio(i) = NaN;        
    end
end
Symm(:,2) = nanmean(ratio);
clear ratio


% Convert cycles times to sample number
cycle = round(cycles.*200);

% Range of motion inside foot cycle
for i=1:length(cycle)-1
range(i) = max(Angle1(cycle(i,1):cycle(i+1,1)))-min(Angle1(cycle(i,1):cycle(i+1,1)));     % Range of motion
end
RoMF1 = statistics(range); % range of motion of left arm during right leg gait cycle
RoMF1 = [RoMF1,range];

for i=1:length(cycle)-1
    try
range(i) = max(Angle2(cycle(i,2):cycle(i+1,2)))-min(Angle2(cycle(i,2):cycle(i+1,2)));     % Range of motion
    catch
        fprintf('error %d',i);
    end
end
RoMF2 = statistics(range); % range of motion of right arm during left leg gait cycle
RoMF2 = [RoMF2,range];

% Absolute range of motion
ARoM1(:,1) = max(Angle1)-min(Angle1);     % Absolute range of motion over whole file
ARoM1(:,2) = max(Angle1);      % Maximum forward angle
ARoM1(:,2) = abs(min(Angle1));      % Maximum backward angle

ARoM2(:,1) = max(Angle2)-min(Angle2);     % Absolute range of motion over whole file
ARoM2(:,2) = max(Angle2);      % Maximum forward angle
ARoM2(:,2) = abs(min(Angle2));      % Maximum backward angle

% Range of motion per arm cycle
for i=1:length(locs1)-1
    rom1(i) = max(Angle1(locs1(i):locs1(i+1)))-min(Angle1(locs1(i):locs1(i+1)));
end
for i=1:length(locs2)-1
    rom2(i) = max(Angle2(locs2(i):locs2(i+1)))-min(Angle2(locs2(i):locs2(i+1)));
end
RoMCycle1 = statistics(rom1);
RoMCycle2 = statistics(rom2);

for i=1:length(locs2)-1
    deb = find(locs1>locs2(i) & locs1<locs2(i+1));
    if length(deb)>1
       deb = deb(2);
    end
       
    if ~isempty(deb) && deb < length(act1)
       ratio(i) = (rom1(deb)-rom2(i))/(rom1(deb)+rom2(i));
    else
       ratio(i) = NaN;        
    end
end
Symm(:,3) = nanmean(ratio);
clear ratio

for i=1:length(locs1)-1
    deb = find(locs2>locs1(i) & locs2<locs1(i+1));
    if length(deb)>1
       deb = deb(2);
    end
    
    if ~isempty(deb) && deb < length(act2)
       ratio(i) = (rom2(deb)-rom1(i))/(rom2(deb)+rom1(i));
    else
       ratio(i) = NaN;        
    end
end
Symm(:,4) = nanmean(ratio);

% Phase Coordination index ARM
for i=1:min(length(locs1),length(locs2))-1
    if locs1(1)<locs2(1)
        phi(i) = 360*(locs2(i)-locs1(i))/(locs1(i+1)-locs1(i));
    else
        phi(i) = 360*(locs1(i)-locs2(i))/(locs2(i+1)-locs2(i));
    end
end

PphiABS = mean(abs(phi-180))*100/180;
PCIarm(1,1) = std(phi)/mean(phi) + PphiABS;
PCIarm(2,1) = PphiABS;
PCIarm(3,1) = std(phi);
PCIarm(4,1) = mean(phi);
PCIarm(5:8,1)= NaN(4,1);
phiarm = phi;
phiarm(phiarm==0)=NaN;
clear phi;

j=1;
% Phase Coordination index Foot
for i=1:length(cycle)-1
    peak = locs1(find(locs1<cycle(i+1,1) & locs1>cycle(i,1)));
    if isempty(peak)
        fprintf('Arm cycle before gait cycle. Arm cycle skipped\n');
    elseif length(peak)==2
        peak = peak(2);
        phi(j) = 360*((cycle(i,1))-peak)/(cycle(i+1,1)-cycle(i,1));
        j = j+1;
    else
        phi(j) = 360*((cycle(i,1))-peak)/(cycle(i+1,1)-cycle(i,1));
        j = j+1;
    end
end
PphiABS = mean(abs(phi-180))*100/180;
PCIal_Left(1,1) = std(phi)/mean(phi) + PphiABS;
PCIal_Left(2,1) = PphiABS;
PCIal_Left(3,1) = std(phi);
PCIal_Left(4,1) = mean(phi);
PCIal_Left(5:8,1)= NaN(4,1);
phialL=phi;
clear phi;

j=1;
for i=1:length(cycle)-1
    peak = locs2(find(locs2<cycle(i+1,2) & locs2>cycle(i,2)));
     if isempty(peak)
        fprintf('Arm cycle before gait cycle. Arm cycle skipped\n');
        elseif length(peak)==2
        peak = peak(2);
        phi(j) = 360*((cycle(i,2))-peak)/(cycle(i+1,2)-cycle(i,2));
        j = j+1;
     else
        phi(j) = 360*((cycle(i,2))-peak)/(cycle(i+1,2)-cycle(i,2));
        j = j+1;
     end
end
PphiABS = mean(abs(phi-180))*100/180;
PCIal_Right(1,1) = std(phi)/mean(phi) + PphiABS;
PCIal_Right(2,1) = PphiABS;
PCIal_Right(3,1) = std(phi);
PCIal_Right(4,1) = mean(phi);
PCIal_Right(5:8,1)= NaN(4,1);
phialR=phi;
clear phi;


% Phase coordination index legs
for i=1:length(cycles)-1
    if cycles(1,1)<cycles(1,2)
        phi(i) = 360*(cycles(i,2)-cycles(i,1))/(cycles(i+1,1)-cycles(i,1));
    else
        phi(i) = 360*(cycles(i,1)-cycles(i,2))/(cycles(i+1,2)-cycles(i,2));
    end
end

PphiABS = mean(abs(phi-180))*100/180;
PCIleg(1,1) = std(phi)/mean(phi) + PphiABS;
PCIleg(2,1) = PphiABS;
PCIleg(3,1) = std(phi);
PCIleg(4,1) = mean(phi);
PCIleg(5:8,1)= NaN(4,1);
phileg = phi;
clear phi;


% format
RoMCycle1 = RoMCycle1';
RoMCycle1(8)=Symm(3);
RoMCycle2 = RoMCycle2';
RoMCycle2(8)=Symm(4);
ArmCycleTime1 = ArmCycleTime1';
ArmCycleTime1(8)=Symm(1);
ArmCycleTime2 = ArmCycleTime2';
ArmCycleTime2(8)=Symm(2);
cy = nan(max([length(rom1),length(rom2),length(act1),length(act2),length(phiarm),length(phialL),length(phialR),length(phileg)]),8);
cy(1:length(rom1),1)= rom1;
cy(1:length(rom2),2)= rom2;
cy(1:length(act1),3)= act1./200;
cy(1:length(act2),4)= act2./200;
cy(1:length(phiarm),5)= phiarm;
cy(1:length(phialL),6)= [phialL]';
cy(1:length(phialR),7)= [phialR]';
cy(1:length(phileg),8)= phileg';
end

