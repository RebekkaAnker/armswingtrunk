function varargout = Main_GaitAnalysis_ArmSwing_Trunk(varargin)
% MAIN_GAITANALYSIS_ARMSWING_TRUNK MATLAB code for Main_GaitAnalysis_ArmSwing_Trunk.fig
%      MAIN_GAITANALYSIS_ARMSWING_TRUNK by itself, creates a new MAIN_GAITANALYSIS_ARMSWING_TRUNK or raises the
%      existing singleton*.
%
%      H = MAIN_GAITANALYSIS_ARMSWING_TRUNK returns the handle to a new MAIN_GAITANALYSIS_ARMSWING_TRUNK or the handle to
%      the existing singleton*.
%
%      MAIN_GAITANALYSIS_ARMSWING_TRUNK('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MAIN_GAITANALYSIS_ARMSWING_TRUNK.M with the given input arguments.
%
%      MAIN_GAITANALYSIS_ARMSWING_TRUNK('Property','Value',...) creates a new MAIN_GAITANALYSIS_ARMSWING_TRUNK or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Main_GaitAnalysis_ArmSwing_Trunk_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Main_GaitAnalysis_ArmSwing_Trunk_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Main_GaitAnalysis_ArmSwing_Trunk

% Last Modified by GUIDE v2.5 02-Mar-2016 09:05:41

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Main_GaitAnalysis_ArmSwing_Trunk_OpeningFcn, ...
                   'gui_OutputFcn',  @Main_GaitAnalysis_ArmSwing_Trunk_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before Main_GaitAnalysis_ArmSwing_Trunk is made visible.
function Main_GaitAnalysis_ArmSwing_Trunk_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Main_GaitAnalysis_ArmSwing_Trunk (see VARARGIN)

% Choose default command line output for Main_GaitAnalysis_ArmSwing_Trunk
handles.output = 'Yes';

% Update handles structure
guidata(hObject, handles);

% Insert custom Title and Text if specified by the user
% Hint: when choosing keywords, be sure they are not easily confused 
% with existing figure properties.  See the output of set(figure) for
% a list of figure properties.
if(nargin > 3)
    for index = 1:2:(nargin-3),
        if nargin-3==index, break, end
        switch lower(varargin{index})
         case 'title'
          set(hObject, 'Name', varargin{index+1});
         case 'string'
          set(handles.text1, 'String', varargin{index+1});
        end
    end
end

% Determine the position of the dialog - centered on the callback figure
% if available, else, centered on the screen
FigPos=get(0,'DefaultFigurePosition');
OldUnits = get(hObject, 'Units');
set(hObject, 'Units', 'pixels');
OldPos = get(hObject,'Position');
FigWidth = OldPos(3);
FigHeight = OldPos(4);
if isempty(gcbf)
    ScreenUnits=get(0,'Units');
    set(0,'Units','pixels');
    ScreenSize=get(0,'ScreenSize');
    set(0,'Units',ScreenUnits);

    FigPos(1)=1/2*(ScreenSize(3)-FigWidth);
    FigPos(2)=2/3*(ScreenSize(4)-FigHeight);
else
    GCBFOldUnits = get(gcbf,'Units');
    set(gcbf,'Units','pixels');
    GCBFPos = get(gcbf,'Position');
    set(gcbf,'Units',GCBFOldUnits);
    FigPos(1:2) = [(GCBFPos(1) + GCBFPos(3) / 2) - FigWidth / 2, ...
                   (GCBFPos(2) + GCBFPos(4) / 2) - FigHeight / 2];
end
FigPos(3:4)=[FigWidth FigHeight];
set(hObject, 'Position', FigPos);
set(hObject, 'Units', OldUnits);

% Make the GUI modal
set(handles.figure1,'WindowStyle','modal')

% UIWAIT makes Main_GaitAnalysis_ArmSwing_Trunk wait for user response (see UIRESUME)
uiwait(handles.figure1);

% --- Outputs from this function are returned to the command line.
function varargout = Main_GaitAnalysis_ArmSwing_Trunk_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% The figure can be deleted now
delete(handles.figure1);

% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.output = get(hObject,'String');

% Update handles structure
guidata(hObject, handles);

folderName = uigetdir('', 'Select the folder containing the data to analyse');

filename{6} = [];
if (handles.checkbox_feet.Value)
    [filename{1}, pathname] = uigetfile(strcat(folderName,'/','*.csv'),'Select the file from the left foot');
    [filename{2}, pathname] = uigetfile(strcat(folderName,'/','*.csv'),'Select the file from the right foot');
end

if (handles.checkbox_trunkG.Value)
    [filename{3}, pathname] = uigetfile(strcat(folderName,'/','*.csv'),'Select the file from the trunk (gait)');
end

if (handles.checkbox_trunkS.Value)
    [filename{4}, pathname] = uigetfile(strcat(folderName,'/','*.csv'),'Select the file from the trunk (sway)');
end

if (handles.checkbox_arms.Value)
    [filename{5}, pathname] = uigetfile(strcat(folderName,'/','*.csv'),'Select the file from the left arm');
    [filename{6}, pathname] = uigetfile(strcat(folderName,'/','*.csv'),'Select the file from the right arm');
end

GaitUp_Analysis_a(filename,pathname); 


% Use UIRESUME instead of delete because the OutputFcn needs
% to get the updated handles structure.
uiresume(handles.figure1);





% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.output = get(hObject,'String');

% Update handles structure
guidata(hObject, handles);

% Use UIRESUME instead of delete because the OutputFcn needs
% to get the updated handles structure.
uiresume(handles.figure1);


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if isequal(get(hObject, 'waitstatus'), 'waiting')
    % The GUI is still in UIWAIT, us UIRESUME
    uiresume(hObject);
else
    % The GUI is no longer waiting, just close it
    delete(hObject);
end


% --- Executes on key press over figure1 with no controls selected.
function figure1_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Check for "enter" or "escape"
if isequal(get(hObject,'CurrentKey'),'escape')
    % User said no by hitting escape
    handles.output = 'No';
    
    % Update handles structure
    guidata(hObject, handles);
    
    uiresume(handles.figure1);
end    
    
if isequal(get(hObject,'CurrentKey'),'return')
    uiresume(handles.figure1);
end    


% --- Executes on button press in checkbox_trunkG.
function checkbox_trunkG_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_trunkG (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.checkbox_feet,'Value',1);
% Hint: get(hObject,'Value') returns toggle state of checkbox_trunkG


% --- Executes on button press in checkbox_trunkS.
function checkbox_trunkS_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_trunkS (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_trunkS


% --- Executes on button press in checkbox_feet.
function checkbox_feet_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_feet (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_feet


% --- Executes on button press in checkbox_arms.
function checkbox_arms_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_arms (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.checkbox_feet,'Value',1);



% Hint: get(hObject,'Value') returns toggle state of checkbox_arms
