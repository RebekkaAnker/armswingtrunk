function [ stat ] = trunk_stat( R,L )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

stat(:,1) = statistics(R(:,1));
stat(:,2) = statistics(R(:,2));
stat(:,3) = statistics(L(:,1));
stat(:,4) = statistics(L(:,2));
stat(end,:)=[];
stat(end+1,1) = ratio(stat(1,1),stat(1,3));
stat(end,2) = ratio(stat(1,2),stat(1,4));
stat(end,3) = - ratio(stat(1,1),stat(1,3));
stat(end,4) = - ratio(stat(1,2),stat(1,4));

end

